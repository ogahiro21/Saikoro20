const PLAYERNUM = 4;
const MOVETIME = 500; //プレイヤーがひとマスを移動する時間
const IMAGESIZE = 25;
const JOBS = ["student", "baseball", "chef", "doctor", "engineer", "musician", "teacher"];
const JOBNAMES = ["学生", "野球選手", "料理人", "医者", "エンジニア", "歌手", "教師"];
const spinImages = [];
let age = 20;         //年代
let monitor;          // ゲーム状態を監視する変数
let players = [];     // プレイヤー情報を持つオブジェクト
let diceFlag = false; // diceButtonを押しても反応しないようにするフラグ
let laps = 1;     // プレイヤーが何周しているのかを管理するフラグ
let firstPlayerId = 4;

/* Playerの位置に従って表示する関数 */
const playerPosition = (id) => {
  let player = players[id];
  let x = 0, y = 0;
  switch (id) {
    case 1:
      x += IMAGESIZE;
      break;
    case 2:
      y += IMAGESIZE;
      break;
    case 3:
      x += IMAGESIZE;
      y += IMAGESIZE;
      break;
    default:
      break;
  }
  /* [Issue] grid20をgrid + ageに変更したい*/
  document.getElementById(player.imageId).style.transform
    = `translate( ${eval("grid" + age + "[player.grid].x + x")}px, ${eval("grid" + age + "[player.grid].y + y")}px )`;
}

/* テキストエリアへ表示する関数 */
const displayText = (text) => {
  let area = document.getElementById("area");
  area.value += "\n" + text;
  area.scrollTop = area.scrollHeight;
}

/* Playerの生成 */
const playerInit = () => {
  let playerNames = JSON.parse(window.sessionStorage.getItem("playerNames"));
  for (let i = 0; i < PLAYERNUM; i++) {
    let playerObject = new Object;
    playerObject.playerName = playerNames[i]; // プレイヤーの名前
    playerObject.imageId = `image${i}`;       // html内のid
    playerObject.money = 10000;               // 所持金
    playerObject.rank = 1;                    // 順位
    playerObject.grid = 0;                    // 何マス目にいるか
    playerObject.jobId = 0;                   // 仕事のid
    playerObject.jobLevel = 0;                // 仕事のレベル
    playerObject.rest = 0;                    // 何回休みか
    playerObject.love = {
      "state": "恋人あり",
      "loveline": 0,
      "children": 0
    };
    players.push(playerObject);
  }
  for (let i = 0; i < PLAYERNUM; i++) {
    playerPosition(i);
  }
  updateBoxes();
  // 次は職業決め
  monitor.state = "Job";
  // monitor.state = "Main"; 
}

/* さいころを回転させるだけの関数 */
const diceSpin = () => {
  let dice = document.getElementById("dice");
  dice.src = spinImages[Math.floor(Math.random() * 6) + 1].src;
  dice.style.transform = `rotate(${Math.floor(Math.random() * 180)}deg)`;
};

/* コマの移動 */
const moveGrid = (diceValue, id, times) => {
  setTimeout(() => {
    players[id].grid += 1;
    playerPosition(id);
    if (players[id].grid > 15) {
      firstPlayerId = id + 1;
      updateAge();
    }
    else if (times < diceValue - 1) {
      moveGrid(diceValue, id, times + 1);
    }
  }, MOVETIME);
}

/* 恋愛イベント */
const loveEvent = (diceValue, id) => {
  let tgt = players[id];
  if (diceValue < 4) { // 失敗
    if (tgt.love.loveline > 0) {
      tgt.love.loveline -= 1;
      displayText("恋人と喧嘩をする．幸福度が1下がる.");
    } else {
      tgt.love.state = "独り身";
      tgt.love.loveline = 0;
      displayText("失恋しました...");
      displayText("次頑張りましょう！");
    }
  } else { // 成功
    if (tgt.love.state == "独り身") {
      tgt.love.state = "恋人あり";
      displayText("新しい恋人ができました！");
    } else {
      tgt.love.loveline += 1;
      displayText("恋人と上手くいく．幸福度が上がりました！");
    }
  }
  // 次のプレイヤー
  nextTurn(id + 1);
}

/* 子供イベント */
const childEvent = (diceButton, id) => {
  switch (diceButton) {
    case 1: // 子供が増える
      displayText("おめでとうございます！");
      displayText("新しい家族が増えました！");
      players[id].love.loveline += 1;
      players[id].love.children += 1;
      break;

    case 2:
      displayText("子供が学校で問題を起こす．10000円支払う．");
      players[id].money -= 10000;
      break;

    case 3:
      displayText("子供が私立高校へ進学．100000円支払う．");
      players[id].money -= 100000;
      break;

    case 4:
      displayText("子供が天才棋士になる．30000円もらう．");
      players[id].money += 30000;
      break;

    case 5:
      displayText("おめでとうございます！");
      displayText("新しい家族が増えました！");
      players[id].love.loveline += 1;
      players[id].love.children += 1;
      break;

    case 6:
      displayText("子供が子役として大ブレイク．200000円もらう．");
      players[id].money += 200000;
      break;

    default:
      displayText("child event error");
      break;
  }
  // 次のプレイヤー
  nextTurn(id + 1);
}

/* 子供大人イベント */
const adultEvent = (diceValue, id) => {
  let tgt = players[id];
  switch (diceValue) {
    case 1:
      displayText("子供が仮想通貨の投資に大成功．30000円もらう");
      tgt.money += 30000;
      break;

    case 2:
      displayText("子供から旅行券をもらう．2回休みになるが100000円もらう．");
      tgt.money += 100000;
      tgt.rest += 2;
      break;

    case 3:
      displayText("子供が結婚．全員から10000円もらい、幸福度が3上昇する．");
      for (let i = 0; i < PLAYERNUM; i++) {
        players[i].money -= 10000;
      }
      tgt.money += 40000;
      tgt.love.loveline += 3;
      break;

    case 4:
      displayText("子供が同じ職業に就く．一段階昇給する．");
      if (tgt.jobLevel < 4) {
        tgt.jobLevel += 1;
      }
      break;

    case 5:
      displayText("子供が起業して成功．40000円もらう．");
      tgt.money += 40000;
      break;

    case 6:
      displayText("子供と日帰り旅行に行く．幸福度が2上がる．");
      tgt.love.loveline += 2;
      break;

    default:
      break;
  }
  nextTurn(id + 1);
}

/* 野球選手の給料 */
const baseballPayment = (diceValue, id) => {
  let tgt = players[id];
  if (diceValue > 3) {
    tgt.money += eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary");
    displayText("【" + tgt.playerName + "さんに ¥" + eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") + "が入りました！】");
  } else {
    displayText("【" + tgt.playerName + "さんには給料が入りませんでした...】");
  }
  payment(id + 1);
}

/* エンジニア・歌手の給料 */
const dicePayment = (diceValue, id) => {
  let tgt = players[id];
  tgt.money += eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") * diceValue;
  displayText("【" + tgt.playerName + "さんに ¥" + eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") * diceValue + "が入りました！】");
  payment(id + 1);
}
/* 給料支払い */
const payment = (id) => {
  if (id >= PLAYERNUM) { //支払い終了
    nextTurn(firstPlayerId);
  } else {
    let tgt = players[id];
    switch (tgt.jobId) {
      case 1: // baseball
        displayText(tgt.playerName + "さんへの支払額を決定します");
        displayText("1 ~ 3: 支払いなし , 4 ~ 6: 給料支払い");
        diceButton(baseballPayment, id, true);
        break;

      case 4: // engineer
      case 5: // musican
        displayText(tgt.playerName + "さんへの支払額を決定します");
        diceButton(dicePayment, id, true);
        break;

      default:
        tgt.money += eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary");
        displayText("【" + tgt.playerName + "さんに ¥" + eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") + "が入りました！】");
        payment(id + 1);
        break;
    }
  }
};

/* 給料日 */
const payday = () => {
  animation("給料日");
  payment(0);
}

/* マスのイベント処理 */
const troutEvent = (id) => {
  if (players[id].grid > 15) { // 年代変更
    return "updateAge";
  }
  else {
    let event = eval("event" + age + "[players[id].grid]");

    displayText(event.name);
    switch (event.type) {
      case "money":
        players[id].money += event.value;
        return "movePlayer";

      case "jump":
        players[id].grid = event.value;
        playerPosition(id);
        return "movePlayer";

      case "rest":
        players[id].rest += event.value;
        return "movePlayer";

      case "money_rest":
        players[id].rest += 1;
        players[id].money += event.value;
        return "movePlayer";

      case "love":
        return "loveEvent";

      case "child":
        return "child";

      case "adult":
        return "adult";

      case "gamble":
        return "gamble";

      case "debt":
        return "debt"

      default:
        return "default";
    }
  }
}

/* 年代の更新 */
const updateAge = () => {
  //順位の判定
  rankCheck();

  //年代イベント
  if (age == 20) {
    //結婚
    animation("結婚");
    for (let i = 0; i < PLAYERNUM; i++) {
      if (players[i].love.state == "恋人あり") {
        players[i].love.state = "既婚";
        displayText(players[i].playerName + "さんは結婚しました");
      }
    }
    setTimeout(() => {
      careerEvent();
    }, MOVETIME * 8);
  } else if (age == 30) {
    animation("転職");
    displayText("【" + players[0].playerName + "さんの職業を決定します】");
    diceButton(changeJob, 0, true);
  } else if (age == 40) {
    //子供の成人
    animation("子供成人");
    for (let i = 0; i < PLAYERNUM; i++) {
      if (players[i].love.children > 0) {
        players[i].money += players[i].love.children * 30000;
        displayText(players[i].playerName + "さんの子供は成人しました");
        displayText(`${players[i].playerName}さんに¥${players[i].love.children * 30000}入りました`);
      }
    }
    setTimeout(() => {
      careerEvent();
    }, MOVETIME * 8);
  } else if (age == 50) {
    ending();
  }
}

/* 転職イベント */
const changeJob = (diceValue, id) => {
  let tmp = players[id].jobId;
  players[id].jobId = diceValue;
  displayText(players[id].playerName + "さんの職業は" + JOBNAMES[diceValue] + "に変わりました！");
  if (tmp != players[id].jobId) {
    players[id].jobLevel -= 1;
  }
  if (id < PLAYERNUM - 1) {
    setTimeout(() => {
      displayText("【" + players[id + 1].playerName + "さんの職業を決定します】");
      diceButton(changeJob, id + 1, true);
    }, MOVETIME);
  } else { // 4回目で昇給イベント呼び出し
    careerEvent();
  }
}

/* 順位判定 */
const rankCheck = () => {
  for (let i = 0; i < PLAYERNUM; i++) {
    players[i].rank = 1;
    for (let j = 0; j < PLAYERNUM; j++) {
      if (players[i].grid < players[j].grid) players[i].rank++;
    }
  }
}

/* プレイヤーごとに順位・昇給情報を表示 */
const careerUpText = (id) => {
  let rank = players[id].rank;
  displayText("【" + players[id].playerName + "さん】");
  displayText("順位: " + rank);
  displayText("1 ~ " + (rank + 1) + ": 1ランク昇格, " + (rank + 2) + " ~ 6: 2ランク昇格");
}

/* 昇給イベントの親関数 */
const careerEvent = () => {
  animation("昇給");
  careerUpText(0);
  diceButton(careerUp, 0, true);
}

/* 昇給イベント */
const careerUp = (diceValue, id) => {
  // 1段階昇給
  if (diceValue <= players[id].rank + 1) {
    if (players[id].jobLevel < 4) {
      players[id].jobLevel += 1;
    }
  }
  // 2段階昇給
  else {
    if (players[id].jobLevel < 3) {
      players[id].jobLevel += 2;
    } else {
      players[id].jobLevel = 4;
    }
  }
  displayText(players[id].playerName + "さんは" + eval(JOBS[players[id].jobId] + "[" + players[id].jobLevel + "].name") + "に昇格しました！");

  if (id < PLAYERNUM - 1) {
    setTimeout(() => {
      careerUpText(id + 1);
      diceButton(careerUp, id + 1, true);
    }, MOVETIME);
  } else {
    ageSet();
  }
}

/* ステージ更新 */
const ageSet = () => {
  age += 10;
  animation(`${age}代`);
  let target = document.getElementsByClassName("game_field")[0];
  target.style.backgroundImage = `url(../public/image/background/background${age}.png`;
  for (i = 0; i < PLAYERNUM; i++) {
    players[i].grid = 0;
    playerPosition(i);
  }
  laps = 0;
  nextTurn(firstPlayerId);
}

/* 次のターンの処理 */
const nextTurn = (id) => {
  if (id == firstPlayerId) {
    laps += 1;
    if (laps == 3) {
      laps = 0;
      payday();
    } else {
      displayText("【" + players[id % 4].playerName + "さんのターンです】");
      diceButton(movePlayer, id % 4);
    }
  } else {
    displayText("【" + players[id % 4].playerName + "さんのターンです】");
    diceButton(movePlayer, id % 4);
  }
}

/* 賭け金の決定 */
const enterBet = (diceValue, id) => {
  let bet = parseInt(players[id].money / diceValue);
  sessionStorage.bet = bet;
  displayText(`賭け金は${bet}円に決まりました！`);
  displayText("それではギャンブルです")
  displayText("1 ~ 3: 賭け金を失う , 4 ~ 6 賭け金 x (2 + 幸福度)を得る");
  diceButton(gambleEvent, id);
}

/* ギャンブル結果 */
const gambleEvent = (diceValue, id) => {
  if (diceValue < 4) {
    displayText("ギャンブル失敗...");
    displayText("賭け金を失いました...");
    players[id].money -= sessionStorage.bet;
  }
  else {
    displayText("ギャンブル成功！");
    players[id].money += parseInt(sessionStorage.bet * (2 + players[id].love.loveline));
  }
  nextTurn(id + 1);
}

/* Playerの移動 */
const movePlayer = (diceValue, id) => {
  let nextEvent = "";
  if (diceValue != 0) {
    // マス移動
    moveGrid(diceValue, id, 0);
    // マスのイベント
    setTimeout(() => {
      nextEvent = troutEvent(id);

    }, MOVETIME * (diceValue + 1));
  }
  setTimeout(() => {
    if (nextEvent == "loveEvent") { // 恋愛イベント
      diceButton(loveEvent, id);
    }
    else if (nextEvent == "child") {
      if (players[id].love.state == "既婚") {
        if (players[id].love.children == 0) {
          displayText("おめでとうございます！");
          displayText("新しい家族が増えました！");
          players[id].love.loveline += 1;
          players[id].love.children += 1;
          nextTurn(id + 1);
        }
        else {
          displayText("サイコロを振ってください！");
          displayText("さいの目に応じて子供イベントが発生します！");
          // 子供イベント発生
          diceButton(childEvent, id);
        }
      }
      else {
        displayText("結婚していれば幸せな家庭を築けていたのかな...");
        nextTurn(id + 1);
      }
    }
    else if (nextEvent == "adult") {
      let tgt = players[id].love;
      if (tgt.children > 0) {
        displayText("サイコロを振ってください！");
        displayText("さいの目に応じてイベントが発生します！");
        // 子供大人イベント発生
        diceButton(adultEvent, id);
      }
      else {
        displayText("子供がいません...");
        nextTurn(id + 1);
      }
    }
    else if (nextEvent == "gamble") {
      if (players[id].money > 0) {
        displayText("掛け金を決定します！サイコロを振ってください．");
        displayText("掛け金 = 全財産 / サイコロの目");
        diceButton(enterBet, id);
      } else {
        displayText("借金中はギャンブルに挑戦できません");
        nextTurn(id + 1);
      }
    }
    else if (nextEvent == "debt") {
      if (players[id].money < 0) {
        displayText("1 ~ 3: 財産が0になる, 4 ~ 6: 財産が-10倍される");
        diceButton(debtEvent, id);
      } else {
        displayText("このイベントは資産がマイナスなプレイヤーのみ発生します");
        nextTurn(id + 1);
      }
    }
    else if (nextEvent != "updateAge") { // 次のターン
      nextTurn(id + 1);
    }
  }, MOVETIME * (diceValue + 2));
};

/* 借金返済イベント */
const debtEvent = (diceValue, id) => {
  if (diceValue < 4) {
    displayText("借金返済！")
    displayText("資産が¥0になりました！");
    players[id].money = 0;
  } else {
    displayText("一発逆転！");
    displayText("資産が-10倍されます!");
    players[id].money *= -10;
  }
  nextTurn(id + 1);
}

/* ゲームスタートのアニメーションなど */
const gameMain = () => {
  animation("START");
  displayText("----------------");
  displayText("ゲームスタート！");
  displayText("----------------");
  displayText("【" + players[0].playerName + "さんのターンです】");
  diceButton(movePlayer, 0);
}

/* さいころボタンが押された時の処理 */
const diceButton = (next, id, status) => {
  updateBoxes();
  let timerId; // setInterval管理用の変数
  let btnTimes = 0; // diceButtonの押された回数
  if (players[id].rest > 0 && !status) {
    diceFlag = false;
    displayText(players[id].playerName + "さんは" + players[id].rest + "回休みです！");
    players[id].rest -= 1;
    next(0, id);
  } else {
    diceFlag = true;
    /* diceButtonが押された時の処理*/
    document.getElementById("diceButton").onclick = () => {
      if (diceFlag == true) {
        let diceSpeed = 30;  // さいころのスピード
        let dice = document.getElementById("dice");
        dice.style.display = "block";
        let top = 150;
        let left = 50;
        dice.style.top = `${top}px`;
        dice.style.left = `${left}px`;
        // 初回クリック時(さいころ回転)
        if (btnTimes == 0) {
          timerId = setInterval("diceSpin()", diceSpeed);
          btnTimes = 1;
        }
        // 2回目クリック時
        else {
          diceFlag = false;
          clearInterval(timerId); //setIntervalを止める
          let count = 0;
          btnTimes = 0;
          // diceSpeedDown関数を再帰呼び出しする
          setTimeout(diceSpeedDown = () => {
            diceSpin();
            top += 10; //さいころの座標を移動
            left += 10;//さいころの座標を移動
            count += 1;
            dice.style.top = `${top}px`;
            dice.style.left = `${left}px`;
            // countが10になったら再帰終わり
            if (count < 10) {
              diceSpeed += 30; // 遅くなるスピード
              setTimeout(diceSpeedDown, diceSpeed)
            }
            else {
              let diceValue = Math.floor(Math.random() * 6) + 1;
              dice.style.transform = `rotate(0deg)`;
              dice.src = `../public/image/dice/${diceValue}.png`;
              setTimeout(() => {
                dice.style.display = "none";
                next(diceValue, id);
              }, 1000)
            }
          }, diceSpeed);
        }
      }
    };
  }
}

/* 職業決定 */
const getJob = (diceValue, id) => {
  players[id].jobId = diceValue;
  displayText(players[id].playerName + "さんの職業は" + JOBNAMES[diceValue] + "になりました！");

  if (id < PLAYERNUM - 1) {
    // callbackで3回diceButtonを呼び出す
    displayText("【" + players[id + 1].playerName + "さんはサイコロを振ってください】");
    diceButton(getJob, id + 1);
  } else {
    // 職業決め終了
    monitor.state = "Main";
  }
}

/* ゲーム状態監視 Object */
const monitorObject = () => {
  let monitor = Object.create(null),
    state = "";
  Object.defineProperty(monitor, 'state', {
    set: function set(value) {
      value = String(value);
      // monitor.state が変更されたら発火
      if (value !== state) {
        state = value;
        switch (value) {
          case "Init":
            playerInit();
            break;

          case "Job":
            animation("職業決め");
            displayText("プレイヤーの職業を決めます．");
            displayText("【" + players[0].playerName + "さんはサイコロを振ってください】");
            diceButton(getJob, 0);
            break;

          case "Main":
            gameMain();
            break;

          default:
            break;
        }
      }
    },
    get: function get() {
      return state;
    }
  });
  return monitor;
}

/* 回転画像を予め読み込む */
const preloadImg = () => {
  spinImages[0] = null;
  for (let i = 1; i < 7; i++) {
    spinImages[i] = new Image();
    spinImages[i].src = `../public/image/dice/spin${i}.png`;
  }
}

window.onload = () => {
  preloadImg();
  monitor = monitorObject();
  monitor.state = "Init";
}

/* サイドメニューの更新 */
const updateBoxes = () => {
  for (let i = 0; i < PLAYERNUM; i++) {
    // Player Name
    document.getElementById(`player${i + 1}Name`).innerHTML = `${players[i].playerName}`;
    // Job name
    document.getElementById(`player${i + 1}Job`).innerHTML = `${JOBNAMES[players[i].jobId]}`;
    // Player money
    document.getElementById(`player${i + 1}Money`).innerHTML = `${players[i].money}`;
    // Job level
    document.getElementById(`player${i + 1}jobLevel`).innerHTML = `${players[i].jobLevel}`;
    // Job level name
    document.getElementById(`player${i + 1}jobLevelName`).innerHTML = eval(JOBS[players[i].jobId] + "[" + players[i].jobLevel + "].name");
    // Love state
    document.getElementById(`player${i + 1}LoveState`).innerHTML = `${players[i].love.state}`;
    // Loveline
    document.getElementById(`player${i + 1}Loveline`).innerHTML = `${players[i].love.loveline}`;
    // Children
    document.getElementById(`player${i + 1}Children`).innerHTML = `${players[i].love.children}`;
  }
}

const hiddenBoxes = () => {
  for (let i = 0; i < PLAYERNUM; i++) {
    // Player money
    document.getElementById(`player${i + 1}Money`).innerHTML = "xxxxx";
  }
}

/* エンディングアニメーション */
const ending = () => {
  let rank;
  let wait = MOVETIME * 8;
  animation("ゲーム終了");
  hiddenBoxes();
  moneyRank();
  setTimeout(() => {
    animation("結果発表");
    displayText("最終順位は資産によって決定します");
  }, wait);
  setTimeout(() => {
    animation("4位は...");
    rank = searchRank(4);
  }, wait * 2);
  setTimeout(() => {
    if (rank.length == 0) {
      animation("いません！");
    } else {
      animation(rank);
      displayText("4位: " + rank + "さん");
    }
  }, wait * 3);
  setTimeout(() => {
    animation("3位は...");
    rank = searchRank(3);
  }, wait * 4);
  setTimeout(() => {
    if (rank.length == 0) {
      animation("いません！");
    } else {
      animation(rank);
      displayText("3位: " + rank + "さん");
    }
  }, wait * 5);
  setTimeout(() => {
    animation("2位は...");
    rank = searchRank(2);
  }, wait * 6);
  setTimeout(() => {
    if (rank.length == 0) {
      animation("いません！");
    } else {
      animation(rank);
      displayText("2位: " + rank + "さん");
    }
  }, wait * 7);
  setTimeout(() => {
    animation("1位は...");
    rank = searchRank(1);
  }, wait * 8);
  setTimeout(() => {
    if (rank.length == 0) {
      animation("いません！");
    } else {
      animation(rank);
      displayText("1位: " + rank + "さん");
    }
  }, wait * 9);
  setTimeout(() => {
    animation("おわり");
  }, wait * 10);
  setTimeout(() => {
    location.href = "./index.html";
  }, wait * 11);
}

/* 順位判定 */
const moneyRank = () => {
  for (let i = 0; i < PLAYERNUM; i++) {
    players[i].rank = 1;
    for (let j = 0; j < PLAYERNUM; j++) {
      if (players[i].money < players[j].money) {
        players[i].rank++;
      }
    }
  }
}

const searchRank = (rank) => {
  let list = [];
  for (let i = 0; i < PLAYERNUM; i++) {
    if (players[i].rank == rank) {
      list.push(players[i].playerName);
    }
  }
  return list;
}