let chef = 
{
    "chef":[
        {
            "name" : "見習い",
            "salary" : 150000
        },

        {
            "name" : "二流ホテルシェフ",
            "salary" : 250000
        },

        {
            "name" : "一流ホテルシェフ",
            "salary" : 300000
        },

        {
            "name" : "三つ星ホテルシェフ",
            "salary" : 400000
        },

        {
            "name" : "総料理長",
            "salary" : 550000
        }
    ]
}