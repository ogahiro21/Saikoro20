let baseball = 
{
    "baseball":[
        {
            "name" : "育成選手",
            "salary" : 150000
        },

        {
            "name" : "2軍選手",
            "salary" : 250000
        },

        {
            "name" : "1軍選手",
            "salary" : 300000
        },

        {
            "name" : "打率3割選手",
            "salary" : 400000
        },

        {
            "name" : "三冠王選手",
            "salary" : 550000
        }
    ]
}