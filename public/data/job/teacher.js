let teacher = 
{
    "teacher":[
        {
            "name" : "講師",
            "salary" : 150000
        },

        {
            "name" : "教諭",
            "salary" : 250000
        },

        {
            "name" : "主任教諭",
            "salary" : 300000
        },

        {
            "name" : "教頭",
            "salary" : 400000
        },

        {
            "name" : "校長",
            "salary" : 550000
        }
    ]
}
