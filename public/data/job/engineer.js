let engineer = 
{
    "engineer":[
        {
            "name" : "平社員",
            "salary" : 150000
        },
    
        {
            "name" : "主任",
            "salary" : 250000
        },
    
        {
            "name" : "課長",
            "salary" : 300000
        },
    
        {
            "name" : "部長",
            "salary" : 400000
        },
    
        {
            "name" : "執行役員",
            "salary" : 550000
        }
    ]
}
