let doctor = 
{
    "doctor":[
        {
            "name" : "研修医",
            "salary" : 150000
        },
    
        {
            "name" : "若手医師",
            "salary" : 250000
        },
    
        {
            "name" : "中堅医師",
            "salary" : 300000
        },
    
        {
            "name" : "ベテラン医師",
            "salary" : 400000
        },
    
        {
            "name" : "院長",
            "salary" : 550000
        }
    ]
}