{
    "event":[
        {
          "type" : null,
          "text" : ""

        },
        {
            "type" : "gold",
            "value" : 10000,
            "text" : "お隣さんと仲良くなる．10000円もらう．"
        },
        {
            "type" : "jump",
            "value" : 5,
            "text" : "ジャンプマス"
        },
        {
            "type" : "gold",
            "value" : -30000,
            "text" : "コンビニで違法駐車．30000円払う"
        },
        {
            "type" : "gold",
            "value" : -39000,
            "text" : "ソーシャルゲームにハマってしまう-39000円はらう．"
        },
        {
            "type" : "gold",
            "value" : 30000,
            "text" : "大会に出場して最優秀賞．30000円もらう．"
        },
        {
            "type" : "gold",
            "value" : -29000,
            "text" : "旅行先で落し物をする．29000円はらう．"
        },
        {
            "type" : "gold",
            "value" : 15000,
            "text" : "通勤途中に人助け．15000円もらう．"
        },
        {
            "type" : "gold",
            "value" : -8000,
            "text" : "飲み会に参加．8000円はらう．"
        },
        {
            "type" : "revenge",
            "text" : "仕返しマス"
        },
        {
            "type" : "love",
            "text" : "恋愛イベント"
        },
        {
            "type" : "gold_rest",
            "value" : 4000,
            "text" : "交通事故の被害にあう．1回休みになるが40000円もらう．"
        },
        {
            "type" : "gold",
            "value" : -125000,
            "text" : "仮想通貨の投資に失敗．125000円はらう．"
        },
        {
            "type" : "gold",
            "value" : -35000,
            "text" : "大先輩の前で失態を犯してしまう．35000円はらう．"
        },
        {
            "type" : "gold_rest",
            "value" : 20000,
            "text" : "有給休暇を満喫．1ターン休むが20000円もらう．"
        },
        {
            "type" : "love",
            "text" : "恋愛イベント"
        },
        {
            "type" : "gold",
            "value" : 35000,
            "text" : "写真コンテストで入賞．35000円もらう．"
        },
        {
            "type" : "gold",
            "value" : -37000,
            "text" : "ドラマに影響され服を爆買い．37000円はらう．"
        },
        {
            "type" : "gold",
            "value" : -30000,
            "text" : "通勤用定期を失う．30000円はらう．"
        },
        {
            "type" : "gold",
            "value" : 17000,
            "text" : "担当の取引が上手くいく．17000円もらう．"
        },
        {
            "type" : "gold",
            "value" : -16000,
            "text" : "身体を壊し通院する．16000円払う．"
        },
        {
            "type" : "gold",
            "value" : 4000,
            "text" : "ご近所さんからお土産をいただく．4000円もらう．"
        },
        {
            "type" : "love",
            "text" : "恋愛イベント"
        },
        {
            "type" : "revenge",
            "value" : -30000,
            "text" : "仕返しマス"
        },
        {
            "type" : "jump",
            "value" : -4,
            "text" : "ジャンプマス"
        },
        {
            "type" : "gold",
            "value" : 50000,
            "text" : "主導した計画が大成功！50000円もらう．"
        },
        {
            "type" : "gold",
            "value" : 60000,
            "text" : "主導した計画が大成功！60000円もらう．"
        },
        {
            "type" : "gold",
            "value" : 70000,
            "text" : "主導した計画が大成功！70000円もらう．"
        },
        {
            "type" : "gold",
            "value" : 80000,
            "text" : "主導した計画が大成功！80000円もらう．"
        },
        {
            "type" : "gold",
            "value" : 90000,
            "text" : "主導した計画が大成功！90000円もらう．"
        },
        {
          "type" : null,
          "text" : ""
        }
    ]
}
