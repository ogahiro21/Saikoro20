let student = [
  {
    name: "卓越した学生"
  }
]

let baseball = [
  {
    name: "新人",
    salary: 15000
  },

  {
    name: "2軍選手",
    salary: 30000
  },

  {
    name: "2軍首位打者",
    salary: 45000
  },

  {
    name: "1軍レギュラー",
    salary: 95000
  },

  {
    name: "トリプルスリー",
    salary: 155000
  }
];

let chef = [
  {
    name: "見習い",
    salary: 10000
  },

  {
    name: "二流シェフ",
    salary: 15000
  },

  {
    name: "ホテルシェフ",
    salary: 30000
  },

  {
    name: "三つ星ホテルシェフ",
    salary: 75000
  },

  {
    name: "総料理長",
    salary: 120000
  }
]


let doctor = [
  {
    name: "研修医",
    salary: 5000
  },

  {
    name: "若手医師",
    salary: 10000
  },

  {
    name: "中堅医師",
    salary: 15000
  },

  {
    name: "ベテラン医師",
    salary: 50000
  },

  {
    name: "院長",
    salary: 200000
  }
]

let engineer = [
  {
    name: "平社員",
    salary: 5000
  },

  {
    name: "主任",
    salary: 7500
  },

  {
    name: "課長",
    salary: 10000
  },

  {
    name: "部長",
    salary: 15000
  },

  {
    name: "執行役員",
    salary: 20000
  }
];

let musician = [
  {
    name: "路上歌手",
    salary: 2000
  },

  {
    name: "ミニヒット歌手",
    salary: 5000
  },

  {
    name: "CDデビュー歌手",
    salary: 7500
  },

  {
    name: "ライブ歌手",
    salary: 15000
  },

  {
    name: "Mステ出場歌手",
    salary: 30000
  }
];

let teacher = [
  {
    name: "講師",
      salary: 45000
  },

  {
    name: "教諭",
      salary: 46000
  },

  {
    name: "主任教諭",
    salary: 47000
  },

  {
    name: "教頭",
    salary: 48000
  },

  {
    name: "校長",
    salary: 50000
  }
];
