const animation = (msg) => {
  document.getElementById("animeText").innerHTML = msg;
  let text = document.getElementById('animation');
  let promise = new Promise((resolve, reject) => {
    text.style.display = "block";
    setTimeout(() => {
      resolve(text.classList.add('fadeout'));
    }, 2000);
  });
  promise.then((msg) => {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        text.style.display = "none";
        resolve();
      }, 1000);
    });
  }).then((msg) => {
    text.classList.remove('fadeout');
  });
}