/* num     ... マス番号(1~15)
   name    ... マス名(処理内容)
   money   ... お金(数値の分だけ増減する)
   rest    ... 休み(数値のだけ休む)
   love    ... 恋愛マス
   jump    ... ジャンプマス
   child   ... 子供マス
   money_rest ... お金が増減 & 休みが発生する
*/
let event20 =
  [
    {
      num: "0",
      type: null
    },
    {
      num: "1",
      name: "お隣さんと仲良くなる．10000円もらう．",
      type: "money",
      value: 10000
    },
    {
      num: "2",
      name: "ソーシャルゲームにハマってしまう-39000円はらう．",
      type: "money",
      value: -39000
    },
    {
      num: "3",
      name: "スクラッチで3等当選．30000円もらう．",
      type: "money",
      value: 30000
    },
    {
      num: "4",
      name: "交通事故の被害にあう．1回休みになるが40000円もらう．",
      type: "money_rest",
      value: 40000
    },
    {
      num: "5",
      name: "恋愛イベント (1 ~ 3 : 失敗，4 ~ 6 :成功)",
      type: "love"

    },
    {
      num: "6",
      name: "旅行先で落し物をする．29000円はらう．",
      type: "money",
      value: -29000
    },
    {
      num: "7",
      name: "通勤途中に人助け．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "8",
      name: "飲みの場で失態を晒す．-37000円はらう．",
      type: "money",
      value: -37000
    },
    {
      num: "9",
      name: "ママ活を始める．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "10",
      name: "有給休暇を満喫．1ターン休むが20000円もらう．",
      type: "money_rest",
      value: 20000,
    },
    {
      num: "11",
      name: "恋愛イベント (1 ~ 3 : 失敗，4 ~ 6 :成功)",
      type: "love"

    },
    {
      num: "12",
      name: "通勤用定期を失う．16000円はらう．",
      type: "money",
      value: -16000
    },
    {
      num: "13",
      name: "スープバーを企画し成功する．20000円もらう",
      type: "money",
      value: 20000
    },
    {
      num: "14",
      name: "恋愛イベント　(1 ~ 3 : 失敗，4 ~ 6 :成功)",
      type: "love"
    },
    {
      num: "15",
      name: "仮想通貨の投資に失敗．70000円はらう．",
      type: "money",
      value: -70000
    }
  ];

let event30 =
  [
    {
      num: "0",
      type: null
    },
    {
      num: "1",
      name: "競馬で当たる．22000円もらう．",
      type: "money",
      value: 22000
    },
    {
      num: "2",
      name: "ガタクタがまさかのお宝．9000円もらう．",
      type: "money",
      value: 9000
    },
    {
      num: "3",
      name: "なぜかYoutuberを目指す．20000円払う",
      type: "money",
      value: -20000
    },
    {
      num: "4",
      name: "叙々苑で値段何も見ずに注文する．35000円はらう．",
      type: "money",
      value: -30000
    },
    {
      num: "5",
      name: "子供イベント(結婚した人のみ)",
      type: "child"
    },
    {
      num: "6",
      name: "旅行先でSuiCaを落とす．15000円はらう．",
      type: "money",
      value: -15000
    },
    {
      num: "7",
      name: "通勤途中に人助け．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "8",
      name: "ジャンプマス(5マス目に戻る)",
      type: "jump",
      value: 5
    },
    {
      num: "9",
      name: "子供イベント(結婚した人のみ)",
      type: "child"
    },
    {
      num: "10",
      name: "自転車がぶっ壊れる．20000円払う",
      type: "money",
      value: -20000
    },
    {
      num: "11",
      name: "有給休暇を満喫．1ターン休むが20000円もらう．",
      type: "money_rest",
      value: 20000,
    },
    {
      num: "12",
      name: "ロト6に当選．40000円もらう．",
      type: "money",
      value: 40000,
    },
    {
      num: "13",
      name: "1日ヒモになる．5000円もらう．",
      type: "money",
      value: 5000,
    },
    {
      num: "14",
      name: "子供イベント(結婚した人のみ)",
      type: "child"
    },
    {
      num: "15",
      name: "ジャンプマス(10マス目に戻る)",
      type: "jump",
      value: 10
    }
  ];

let event40 =
  [
    {
      num: "0",
      type: null
    },
    {
      num: "1",
      name: "ギックリ腰になる．10000円払って一回休む．",
      type: "money_rest",
      value: -10000
    },
    {
      num: "2",
      name: "雑誌の懸賞で大当たり．22000円もらう．",
      type: "money",
      value: 22000
    },
    {
      num: "3",
      name: "シャンプーと脱毛剤をを間違える．29000円払う．",
      type: "money",
      value: -29000
    },
    {
      num: "4",
      name: "仕事から逃げ出して恥を得たが役に立った．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "5",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "6",
      name: "旅行先でJCBギフト券を拾う．9000円もらう．",
      type: "money",
      value: 9000
    },
    {
      num: "7",
      name: "ポッキーの日にトッポを食べて有罪．20000円払う．",
      type: "money",
      value: -20000
    },
    {
      num: "8",
      name: "お隣さんと一緒に帰って噂される．20000円払う．",
      type: "money",
      value: -35000
    },
    {
      num: "9",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "10",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "11",
      name: "有給休暇を満喫．1ターン休むが10000円もらう．",
      type: "money_rest",
      value: 10000,
    },
    {
      num: "12",
      name: "クレジットカードを紛失．20000円払う．",
      type: "money",
      value: -20000,
    },
    {
      num: "13",
      name: "タンスの中からへそくりが見つかる．40000円もらう．",
      type: "money",
      value: 40000,
    },
    {
      num: "14",
      name: "お正月ハガキ宝くじで当選．20000円もらう．",
      type: "money",
      value: 20000,
    },
    {
      num: "15",
      name: "自然災害で自宅が被害にあう．100000円払う",
      type: "money",
      value: -100000
    }
  ]

let event50 = [
  {
    num: "0",
    type: null
  },
  {
    "num": "1",
    "name": "TikTokをやったら大ウケ．10000円もらう．",
    "type": "money",
    "value": 10000
  },
  {
    "num": "2",
    "name": "ぼーっと生きてんじゃねぇーよ．39000円払う．",
    "type": "money",
    "value": -39000
  },
  {
    "num": "3",
    "name": "お出かけ途中に悪質タックルにあう．1回休むが，30000円もらう．",
    "type": "money_rest",
    "value": 30000
  },
  {
    "num": "4",
    "name": "飲みの場でひょっこりしたらドン引きされる．20000円払う．",
    "type": "money",
    "value": -20000
  },
  {
    "num": "5",
    "name": "雑誌の懸賞で大当たりする．40000円もらう．",
    "type": "money",
    "value": 40000
  },
  {
    "num": "6",
    "name": "ギャンブルマス",
    "type": "gamble"
  },
  {
    "num": "7",
    "name": "ギャンブルマス",
    "type": "gamble"
  },
  {
    "num": "8",
    "name": "ギャンブルマス",
    "type": "gamble"
  },
  {
    "num": "9",
    "name": "立とうとしたら腰がやられる．20000円払う．",
    "type": "money",
    "value": -20000
  },
  {
    "num": "10",
    "name": "趣味で運動を始めて健康維持．9000円もらう．",
    "type": "money",
    "value": 9000
  },
  {
    "num": "11",
    "name": "足を骨折．1回休むが，保険金40000円もらう．",
    "type": "money_rest",
    "value": 40000
  },
  {
    "num": "12",
    "name": "若い女と遊んでるのがバレる．35000円払う．",
    "type": "money",
    "value": -35000
  },
  {
    "num": "13",
    "name": "借金返済イベント(資産がマイナスの人のみ)",
    "type": "debt"
  },
  {
    "num": "14",
    "name": "「隣の晩御飯」でヨネスケが来る．120000円もらう．",
    "type": "money",
    "value": 120000
  },
  {
    "num": "15",
    "name": "煽り運転容疑で逮捕，245000円払う．",
    "type": "money",
    "value": -245000
  }
]