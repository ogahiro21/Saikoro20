##  `/views/index.html`

```html
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">

  <link rel="stylesheet" type="text/css" href="../public/stylesheets/style.css">
  <link rel="stylesheet" type="text/css" href="../public/stylesheets/register.css">
  <script type="text/javascript" src="../controllers/register.js"></script>

</head>

<body>
  <title>すごろくは20歳になってから</title>
  <iframe src="./header.html"></iframe>

  <h3> プレイヤー名を登録</h3>
  <div class="register_form" id=register_form>
    <!-- input type で入力の形式を指定する -->
    <p>Player1 <img id="face0" src="../public/image/denden.png"> <input class="playerNames" type="text" name="player1" /></p>
    <p>Player2 <img id="face1" src="../public/image/usagi.png">  <input class="playerNames" type="text" name="player2" /></p>
    <p>Player3 <img id="face2" src="../public/image/kani.png">   <input class="playerNames" type="text" name="player3" /></p>
    <p>Player4 <img id="face3" src="../public/image/hiyoko.png"> <input class="playerNames" type="text" name="player4" /></p>
  </div>
  <a href="#" class="square_btn " onclick="gameStart()">Game Start</a>
</body>

</html>
```

## `/views/header.html`
```html
<!DOCTYPE html>
<html>
  <meta charset="utf-8"/>
  <body style="margin:0;">
  <link rel="stylesheet" type="text/css" href="../public/stylesheets/header.css">
  <section id="header" class="inner">
    <img src="../public/image/dice/1.png" alt="logo" class="logo" />
    <img src="../public/image/title.png" alt="title" class="title" />
  </section>
</body>
</html>
```

## `/views/game.htnl`
```html
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">

  <!-- Link style sheet -->
  <link rel="stylesheet" type="text/css" href="../public/stylesheets/style.css">
  <link rel="stylesheet" type="text/css" href="../public/stylesheets/game.css">
  <!-- Link script -->
  <script type="text/javascript" src="../public/javascript/jobs.js"></script>
  <script type="text/javascript" src="../public/javascript/grid.js"></script>
  <script type="text/javascript" src="../public/javascript/event.js"></script>
  <script type="text/javascript" src="../controllers/game.js"></script>
  <!-- PayDayのアニメーション用 -->
  <script src="../public/javascript/animation.js"></script>
  <link rel="stylesheet" href="../public/stylesheets/animation.css">
  <!--header -->
  <iframe src="./header.html"></iframe>

</head>

<body>
  <div class="boxes">
    <div class="statusBox">
      <div class="box" id="box1">
        <p>
          <span>Name: </span>
          <span id="player1Name">ksk</span>
        </p>
        <p>
          <span>Job: </span>
          <span id="player1Job">野球選手</span>
        </p>
        <p>
          <span>Lv: </span>
          <span id="player1jobLevel">1</span>
          <span id="player1jobLevelName">新人</span>
        </p>
        <p>
          <span>所持金: ¥</span>
          <span id="player1Money">10000</span>
        </p>
        <p>
          <span>恋愛状態: </span>
          <span id="player1LoveState"></span>
        </p>
        <p>
          <span>親密度: </span>
          <span id="player1Loveline"></span>
        </p>
      </div>
      <div class="box" id="box3">
        <p>
          <span>Name: </span>
          <span id="player3Name">koki</span>
        </p>
        <p>
          <span>Job: </span>
          <span id="player3Job">野球選手</span>
        </p>
        <p>
          <span>Lv: </span>
          <span id="player3jobLevel">1</span>
          <span id="player3jobLevelName">新人</span>
        </p>
        <p>
          <span>所持金: ¥</span>
          <span id="player3Money">10000</span>
        </p>
        <p>
          <span>恋愛状態: </span>
          <span id="player3LoveState"></span>
        </p>
        <p>
          <span>親密度: </span>
          <span id="player3Loveline"></span>
        </p>
      </div>
    </div>
    <div class="statusBox" id="center">
      <div class="game_field">
        <img id="dice" src="../public/image/dice/1.png" alt="dice">
        <img id="image0" src="../public/image/denden.png">
        <img id="image1" src="../public/image/usagi.png">
        <img id="image2" src="../public/image/kani.png">
        <img id="image3" src="../public/image/hiyoko.png">
        <div id="animation">
          <p id="animeText">職業選択</p>
        </div>
      </div>

      <div class="bottom-menu">
        <!-- 画面下のメニューエリア -->
        <div class="bottom-menu">
          <!-- 左下のテキストエリア -->
          <textarea id="area" rows="4" name="line" readonly> Options are displayed here </textarea>
          <!-- 右下のコントロールパネル -->
          <div class="controlPanel">
            <table>
              <tbody>
                <tr>
                  <td>
                    <button class="menu-top-left" id="diceButton">サイコロ</button>
                  </td>
                  <td>
                    <button class="menu-top-right">ステータス</button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <button class="menu-bottom-left">順位</button>
                  </td>
                  <td>
                    <button class="menu-bottom-right">設定</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="statusBox">
      <div class="box" id="box2">
        <p>
          <span>Name: </span>
          <span id="player2Name">nene</span>
        </p>
        <p>
          <span>Job: </span>
          <span id="player2Job">野球選手</span>
        </p>
        <p>
          <span>Lv: </span>
          <span id="player2jobLevel">1</span>
          <span id="player2jobLevelName">新人</span>
        </p>
        <p>
          <span>所持金: ¥</span>
          <span id="player2Money">10000</span>
        </p>
        <p>
          <span>恋愛状態: </span>
          <span id="player2LoveState"></span>
        </p>
        <p>
          <span>親密度: </span>
          <span id="player2Loveline"></span>
        </p>
      </div>
      <div class="box" id="box4">
        <p>
          <span>Name: </span>
          <span id="player4Name">tt</span>
        </p>
        <p>
          <span>Job: </span>
          <span id="player4Job">野球選手</span>
        </p>
        <p>
          <span>Lv: </span>
          <span id="player4jobLevel">1</span>
          <span id="player4jobLevelName">新人</span>
        </p>
        <p>
          <span>所持金: ¥</span>
          <span id="player4Money">10000</span>
        </p>
        <p>
          <span>恋愛状態: </span>
          <span id="player4LoveState"></span>
        </p>
        <p>
          <span>親密度: </span>
          <span id="player4Loveline"></span>
        </p>
      </div>
    </div>
  </div>
</body>
</html>
```

## `/controllers/register.js`

```js
const gameStart = () => {
  let nameList = document.getElementsByClassName("playerNames");
  console.log(nameList[0].value);
  sessionStorage.playerNames = JSON.stringify([nameList[0].value, nameList[1].value, nameList[2].value, nameList[3].value]);
  location.href = "./game.html";
}
```

## `/controllers/game.js`

```js
const PLAYERNUM = 4;
const MOVETIME = 500; //プレイヤーがひとマスを移動する時間
const IMAGESIZE = 25;
const JOBS = ["student", "baseball", "chef", "doctor", "engineer", "musician", "teacher"];
const JOBNAMES = ["学生", "野球選手", "料理人", "医者", "エンジニア", "歌手", "教師"];
let age = 20;         //年代
let monitor;          // ゲーム状態を監視する変数
let players = [];     // プレイヤー情報を持つオブジェクト
let diceFlag = false; // diceButtonを押しても反応しないようにするフラグ
let turnFlag = 0;     // プレイヤーが何周しているのかを管理するフラグ
let nextTurnFlag = 0;

/* Playerの位置に従って表示する関数 */
const playerPosition = (id) => {
  let player = players[id];
  let x = 0, y = 0;
  switch (id) {
    case 1:
      x += IMAGESIZE;
      break;
    case 2:
      y += IMAGESIZE;
      break;
    case 3:
      x += IMAGESIZE;
      y += IMAGESIZE;
      break;
    default:
      break;
  }
  /* [Issue] grid20をgrid + ageに変更したい*/
  document.getElementById(player.imageId).style.transform
    = `translate( ${eval("grid" + age + "[player.grid].x + x")}px, ${eval("grid" + age + "[player.grid].y + y")}px )`;
}

/* テキストエリアへ表示する関数 */
const displayText = (text) => {
  let area = document.getElementById("area");
  area.value += "\n" + text;
  area.scrollTop = area.scrollHeight;
}

/* Playerの生成 */
const playerInit = () => {
  console.log("playerInit");
  let playerNames = JSON.parse(window.sessionStorage.getItem("playerNames"));
  for (let i = 0; i < PLAYERNUM; i++) {
    let playerObject = new Object;
    playerObject.playerName = playerNames[i]; // プレイヤーの名前
    playerObject.imageId = `image${i}`;       // html内のid
    playerObject.money = 10000;               // 所持金
    playerObject.rank = 1;                    // 順位
    playerObject.grid = 0;                    // 何マス目にいるか
    playerObject.jobId = 0;                   // 仕事のid
    playerObject.jobLevel = 0;                // 仕事のレベル
    playerObject.rest = 0;                    // 何回休みか
    playerObject.love = {
      "state": "恋人あり",
      "loveline": 0,
      "children": 0
    };
    players.push(playerObject);
  }
  for (let i = 0; i < PLAYERNUM; i++) {
    playerPosition(i);
  }
  updateBoxes();
  // 次は職業決め
  monitor.state = "Job";
  // monitor.state = "Main";
}

/* さいころを回転させるだけの関数 */
const diceSpin = () => {
  let dice = document.getElementById("dice");
  dice.src = `../public/image/dice/spin${Math.floor(Math.random() * 6) + 1}.png`;
  dice.style.transform = `rotate(${Math.floor(Math.random() * 180)}deg)`;
};

/* コマの移動 */
const moveGrid = (diceValue, id, times) => {
  setTimeout(() => {
    players[id].grid += 1;
    playerPosition(id);
    if (players[id].grid > 15) {
      nextTurnFlag = id + 1;
      updateAge();
    }
    else if (times < diceValue - 1) {
      moveGrid(diceValue, id, times + 1);
    }
  }, MOVETIME);
}

/* 恋愛イベント */
const loveEvent = (diceValue, id) => {
  let tgt = players[id];
  if (diceValue < 4) { // 失敗
    if (tgt.love.loveline > 0) {
      tgt.love.loveline -= 1;
      displayText("恋人との親密度が下がりました...");
    } else {
      tgt.love.state = "独り身";
      tgt.love.loveline = 0;
      displayText("失恋しました...");
      displayText("次頑張りましょう！");
    }
  } else { // 成功
    if (tgt.love.state == "独り身") {
      tgt.love.state = "恋人あり";
      displayText("新しい恋人ができました！");
    } else {
      tgt.love.loveline += 1;
      displayText("恋人との親密度が上がりました！");
    }
  }
  // 次のプレイヤー
  nextTurn(id + 1);
}

/* 子供イベント */
const addChild = (id) => {
  if (players[id].love.state == "既婚") {
    displayText("おめでとうございます！");
    displayText("新しい家族が増えました！");
    players[id].love.children += 1;
  } else {
    displayText("結婚していれば幸せな家庭を築けていたのかな...");
  }
}

/* 野球選手の給料 */
const baseballPayment = (diceValue, id) => {
  let tgt = players[id];
  if (diceValue > 3) {
    tgt.money += eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary");
    displayText("【" + tgt.playerName + "さんに ¥" + eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") + "が入りました！】");
  } else {
    displayText("【" + tgt.playerName + "さんには給料が入りませんでした...】");
  }
  payment(id + 1);
}

/* エンジニア・歌手の給料 */
const dicePayment = (diceValue, id) => {
  let tgt = players[id];
  tgt.money += eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") * diceValue;
  displayText("【" + tgt.playerName + "さんに ¥" + eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") * diceValue + "が入りました！】");
  payment(id + 1);
}
/* 給料支払い */
const payment = (id) => {
  if (id >= PLAYERNUM) { //支払い終了
    nextTurn(0);
  } else {
    let tgt = players[id];
    switch (tgt.jobId) {
      case 1: // baseball
        displayText(tgt.playerName + "さんへの支払額を決定します");
        displayText("1 ~ 3: 支払いなし , 4 ~ 6: 給料支払い");
        diceButton(baseballPayment, id, true);
        break;

      case 4: // engineer
      case 5: // musican
        displayText(tgt.playerName + "さんへの支払額を決定します");
        diceButton(dicePayment, id, true);
        break;

      default:
        tgt.money += eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary");
        displayText("【" + tgt.playerName + "さんに ¥" + eval(JOBS[tgt.jobId] + "[" + tgt.jobLevel + "].salary") + "が入りました！】");
        payment(id + 1);
        break;
    }
  }
};

/* 給料日 */
const payday = () => {
  animation("給料日");
  payment(0);
}

/* マスのイベント処理 */
const troutEvent = (id) => {
  if (players[id].grid > 15) { // 年代変更
    return "updateAge";
  }
  else {
    let event = eval("event" + age + "[players[id].grid]");

    displayText(event.name);
    switch (event.type) {
      case "money":
        players[id].money += event.value;
        return "movePlayer";

      case "jump":
        players[id].grid = event.value;
        playerPosition(id);
        return "movePlayer";

      case "rest":
        players[id].rest += event.value;
        return "movePlayer";

      case "money_rest":
        players[id].rest += 1;
        players[id].money += event.value;
        return "movePlayer";

      case "love":
        return "loveEvent";

      case "child":
        addChild(id);
        return "movePlayer";


      default:
        return "default";
    }
  }
}

/* 年代の更新 */
const updateAge = () => {
  //順位の判定
  rankCheck();

  //年代イベント
  if (age == 20) {
    //結婚
    animation("結婚");
    for (let i = 0; i < PLAYERNUM; i++) {
      if (players[i].love.state == "恋人あり") {
        players[i].love.state = "既婚";
        displayText(players[i].playerName + "さんは結婚しました");
      }
    }
    setTimeout(() => {
      careerEvent();
    }, MOVETIME * 6);
  } else if (age == 30) {
    animation("転職イベント");
    diceButton(changeJob, 0, true);
  } else if (age == 40) {
    //子供の成人
    animation("子供の成人");
    for (let i = 0; i < PLAYERNUM; i++) {
      if (players[i].love.children > 0) {
        displayText(players[i].playerName + "さんの子供は成人しました");
      }
    }
    setTimeout(() => {
      careerEvent();
    }, MOVETIME * 6);
  }
}

/* 転職イベント */
const changeJob = (diceValue, id) => {
  let tmp = players[id].jobId;
  players[id].jobId = diceValue;
  displayText(players[id].playerName + "さんの職業は" + JOBNAMES[diceValue] + "に変わりました！");
  if (tmp != players[id].jobId) {
    players[id].jobLevel -= 1;
  }
  if (id < PLAYERNUM - 1) {
    diceButton(changeJob, id + 1, true);
  } else { // 4回目で昇給イベント呼び出し
    careerEvent();
  }
}

/* 順位判定 */
const rankCheck = () => {
  for (let i = 0; i < PLAYERNUM; i++) {
    players[i].rank = 1;
    for (let j = 0; j < PLAYERNUM; j++) {
      if (players[i].grid < players[j].grid) players[i].rank++;
    }
  }
  for (i = 0; i < PLAYERNUM; i++) {
    displayText(players[i].playerName + "さんは" + players[i].rank + "位です");
  }
}

/* 昇給イベントの親関数 */
const careerEvent = () => {
  animation("昇給");
  for (let i = 0; i < PLAYERNUM; i++) {
    displayText("【" + (i + 1) + "位 1~" + (i + 2) + ":1ランク昇格 " + (i + 3) + "~6:2ランク昇格】");
  }
  diceButton(careerUp, 0, true);
}

/* 昇給イベント */
const careerUp = (diceValue, id) => {
  if (diceValue <= players[id].rank + 1) {
    players[id].jobLevel += 1;
  } else {
    players[id].jobLevel += 2;
  }
  if (players[id].joblevel > 4) {
    players[id].joblevel = 4;
  } else {
    displayText(players[id].playerName + "さんは" + eval(JOBS[players[id].jobId] + "[" + players[id].jobLevel + "].name") + "に昇格しました！");
  }

  if (id < PLAYERNUM - 1) {
    diceButton(careerUp, id + 1, true);
  } else {
    ageSet();
  }
}

/* ステージ更新 */
const ageSet = () => {
  age += 10;
  animation(`${age}代`);
  let target = document.getElementsByClassName("game_field")[0];
  target.style.backgroundImage = `url(../public/image/background/background${age}.png`;
  for (i = 0; i < PLAYERNUM; i++) {
    players[i].grid = 0;
    playerPosition(i);
  }
  turnFlag = 0;
  nextTurn(nextTurnFlag % 4);
}

/* 次のターンの処理 */
const nextTurn = (id) => {
  if (id == 4) {
    turnFlag += 1;
    if (turnFlag % 2 == 0) {
      payday();
    } else {
      nextTurn(0);
    }
  } else {
    displayText("【" + players[id % 4].playerName + "さんのターンです】");
    diceButton(movePlayer, id % 4);
  }
}

/* Playerの移動 */
const movePlayer = (diceValue, id) => {
  let nextEvent = "";
  if (diceValue != 0) {
    // マス移動
    moveGrid(diceValue, id, 0);
    // マスのイベント
    setTimeout(() => {
      nextEvent = troutEvent(id);

    }, MOVETIME * (diceValue + 1));
  }
  setTimeout(() => {
    if (nextEvent == "loveEvent") { // 恋愛イベント
      diceButton(loveEvent, id);
    }
    else if (nextEvent != "updateAge") { // 次のターン
      nextTurn(id + 1);
    }
  }, MOVETIME * (diceValue + 2));
};

/* ゲームスタートのアニメーションなど */
const gameMain = () => {
  animation("START");
  displayText("----------------");
  displayText("ゲームスタート！");
  displayText("----------------");
  displayText("【" + players[0].playerName + "さんのターンです】");
  diceButton(movePlayer, 0);
}

/* さいころボタンが押された時の処理 */
const diceButton = (next, id, status) => {
  console.log("diceButton");
  updateBoxes();
  let timerId; // setInterval管理用の変数
  let btnTimes = 0; // diceButtonの押された回数
  if (players[id].rest > 0 && !status) {
    diceFlag = false;
    displayText(players[id].playerName + "さんは" + players[id].rest + "回休みです！");
    players[id].rest -= 1;
    next(0, id);
  } else {
    diceFlag = true;
    /* diceButtonが押された時の処理*/
    document.getElementById("diceButton").onclick = () => {
      if (diceFlag == true) {
        let diceSpeed = 30;  // さいころのスピード
        let dice = document.getElementById("dice");
        dice.style.display = "block";
        let top = 150;
        let left = 50;
        dice.style.top = `${top}px`;
        dice.style.left = `${left}px`;
        // 初回クリック時(さいころ回転)
        if (btnTimes == 0) {
          timerId = setInterval("diceSpin()", diceSpeed);
          btnTimes = 1;
        }
        // 2回目クリック時
        else {
          diceFlag = false;
          clearInterval(timerId); //setIntervalを止める
          let count = 0;
          btnTimes = 0;
          // diceSpeedDown関数を再帰呼び出しする
          setTimeout(diceSpeedDown = () => {
            diceSpin();
            top += 10; //さいころの座標を移動
            left += 10;//さいころの座標を移動
            count += 1;
            dice.style.top = `${top}px`;
            dice.style.left = `${left}px`;
            // countが10になったら再帰終わり
            if (count < 10) {
              diceSpeed += 30; // 遅くなるスピード
              setTimeout(diceSpeedDown, diceSpeed)
            }
            else {
              let diceValue = Math.floor(Math.random() * 6) + 1;
              dice.style.transform = `rotate(0deg)`;
              dice.src = `../public/image/dice/${diceValue}.png`;
              setTimeout(() => {
                dice.style.display = "none";
                next(diceValue, id);
              }, 1000)
            }
          }, diceSpeed);
        }
      }
    };
  }
}

/* 職業決定 */
const getJob = (diceValue, id) => {
  players[id].jobId = diceValue;
  displayText(players[id].playerName + "さんの職業は" + JOBNAMES[diceValue] + "になりました！");

  if (id < PLAYERNUM - 1) {
    // callbackで3回diceButtonを呼び出す
    diceButton(getJob, id + 1);
  } else {
    // 職業決め終了
    monitor.state = "Main";
  }
}

/* ゲーム状態監視 Object */
const monitorObject = () => {
  let monitor = Object.create(null),
    state = "";
  Object.defineProperty(monitor, 'state', {
    set: function set(value) {
      value = String(value);
      // monitor.state が変更されたら発火
      if (value !== state) {
        state = value;
        switch (value) {
          case "Init":
            playerInit();
            break;

          case "Job":
            animation("職業決め");
            diceButton(getJob, 0);
            break;

          case "Main":
            gameMain();
            break;

          default:
            break;
        }
      }
    },
    get: function get() {
      return state;
    }
  });
  return monitor;
}

window.onload = () => {
  monitor = monitorObject();
  monitor.state = "Init";
}

const updateBoxes = () => {
  for (let i = 0; i < PLAYERNUM; i++) {
    // Player Name
    document.getElementById(`player${i + 1}Name`).innerHTML = `${players[i].playerName}`;
    // Job name
    document.getElementById(`player${i + 1}Job`).innerHTML = `${JOBNAMES[players[i].jobId]}`;
    // Player money
    document.getElementById(`player${i + 1}Money`).innerHTML = `${players[i].money}`;
    // Job level
    document.getElementById(`player${i + 1}jobLevel`).innerHTML = `${players[i].jobLevel}`;
    // Job level name
    document.getElementById(`player${i + 1}jobLevelName`).innerHTML = eval(JOBS[players[i].jobId] + "[" + players[i].jobLevel + "].name");
    // Love state
    document.getElementById(`player${i + 1}LoveState`).innerHTML = `${players[i].love.state}`;
    // Loveline
    document.getElementById(`player${i + 1}Loveline`).innerHTML = `${players[i].love.loveline}`;
  }
}
```

## `/public/javascript/animation.js`

```js
const animation = (msg) => {
  document.getElementById("animeText").innerHTML = msg;
  let text = document.getElementById('animation');
  let promise = new Promise((resolve, reject) => {
    text.style.display = "block";
    setTimeout(() => {
      resolve(text.classList.add('fadeout'));
    }, 2000);
  });
  promise.then((msg) => {
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        text.style.display = "none";
        resolve();
      }, 1000);
    });
  }).then((msg) => {
    text.classList.remove('fadeout');
  });
}
```

## `/public/javascript/event.js`

```js
/* num     ... マス番号(1~15)
   name    ... マス名(処理内容)
   money   ... お金(数値の分だけ増減する)
   rest    ... 休み(数値のだけ休む)
   love    ... 恋愛マス
   jump    ... ジャンプマス
   child   ... 子供マス
   money_rest ... お金が増減 & 休みが発生する
*/
let event20 =
  [
    {
      num: "0",
      type: null
    },
    {
      num: "1",
      name: "お隣さんと仲良くなる．10000円もらう．",
      type: "money",
      value: 10000
    },
    {
      num: "2",
      name: "ソーシャルゲームにハマってしまう-39000円はらう．",
      type: "money",
      value: -39000
    },
    {
      num: "3",
      name: "スクラッチで3等当選．30000円もらう．",
      type: "money",
      value: 30000
    },
    {
      num: "4",
      name: "交通事故の被害にあう．1回休みになるが40000円もらう．",
      type: "money_rest",
      value: 40000
    },
    {
      num: "5",
      name: "恋愛イベント (1 ~ 3 : 失敗，4 ~ 6 :成功)",
      type: "love"

    },
    {
      num: "6",
      name: "旅行先で落し物をする．29000円はらう．",
      type: "money",
      value: -29000
    },
    {
      num: "7",
      name: "通勤途中に人助け．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "8",
      name: "飲みの場で失態を晒す．-37000円はらう．",
      type: "money",
      value: -37000
    },
    {
      num: "9",
      name: "ママ活を始める．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "10",
      name: "有給休暇を満喫．1ターン休むが20000円もらう．",
      type: "money_rest",
      value: 20000,
    },
    {
      num: "11",
      name: "恋愛イベント (1 ~ 3 : 失敗，4 ~ 6 :成功)",
      type: "love"

    },
    {
      num: "12",
      name: "通勤用定期を失う．16000円はらう．",
      type: "money",
      value: -16000
    },
    {
      num: "13",
      name: "スープバーを企画し成功する．20000円もらう",
      type: "money",
      value: 20000
    },
    {
      num: "14",
      name: "恋愛イベント　(1 ~ 3 : 失敗，4 ~ 6 :成功)",
      type: "love"
    },
    {
      num: "15",
      name: "仮想通貨の投資に失敗．70000円はらう．",
      type: "money",
      value: -70000
    }
  ]

let event30 =
  [
    {
      num: "0",
      type: null
    },
    {
      num: "1",
      name: "競馬で当たる．22000円もらう．",
      type: "money",
      value: 22000
    },
    {
      num: "2",
      name: "ガタクタがまさかのお宝．9000円もらう．",
      type: "money",
      value: 9000
    },
    {
      num: "3",
      name: "なぜかYoutuberを目指す．20000円払う",
      type: "money",
      value: -20000
    },
    {
      num: "4",
      name: "叙々苑で値段何も見ずに注文する．35000円はらう．",
      type: "money",
      value: -30000
    },
    {
      num: "5",
      name: "子供イベント(結婚した人のみ)",
      type: "child"
    },
    {
      num: "6",
      name: "旅行先でSuiCaを落とす．15000円はらう．",
      type: "money",
      value: -15000
    },
    {
      num: "7",
      name: "通勤途中に人助け．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "8",
      name: "ジャンプマス(5マス目に戻る)",
      type: "jump",
      value: 5
    },
    {
      num: "9",
      name: "子供イベント(結婚した人のみ)",
      type: "child"
    },
    {
      num: "10",
      name: "自転車がぶっ壊れる．20000円払う",
      type: "money",
      value: -20000
    },
    {
      num: "11",
      name: "有給休暇を満喫．1ターン休むが20000円もらう．",
      type: "money_rest",
      value: 20000,
    },
    {
      num: "12",
      name: "ロト6に当選．40000円もらう．",
      type: "money",
      value: 40000,
    },
    {
      num: "13",
      name: "1日ヒモになる．5000円もらう．",
      type: "money",
      value: 5000,
    },
    {
      num: "14",
      name: "子供イベント(結婚した人のみ)",
      type: "child"
    },
    {
      num: "15",
      name: "ジャンプマス(10マス目に戻る)",
      type: "jump",
      value: 10
    }
  ];

let event40 =
  [
    {
      num: "0",
      type: null
    },
    {
      num: "1",
      name: "ギックリ腰になる．10000円払って一回休む．",
      type: "money_rest",
      value: -10000
    },
    {
      num: "2",
      name: "雑誌の懸賞で大当たり．22000円もらう．",
      type: "money",
      value: 22000
    },
    {
      num: "3",
      name: "シャンプーと脱毛剤をを間違える．29000円払う．",
      type: "money",
      value: -29000
    },
    {
      num: "4",
      name: "仕事から逃げ出して恥を得たが役に立った．15000円もらう．",
      type: "money",
      value: 15000
    },
    {
      num: "5",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "6",
      name: "旅行先でJCBギフト券を拾う．9000円もらう．",
      type: "money",
      value: 9000
    },
    {
      num: "7",
      name: "ポッキーの日にトッポを食べて有罪．20000円払う．",
      type: "money",
      value: -20000
    },
    {
      num: "8",
      name: "お隣さんと一緒に帰って噂される．20000円払う．",
      type: "money",
      value: -35000
    },
    {
      num: "9",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "10",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "11",
      name: "有給休暇を満喫．1ターン休むが10000円もらう．",
      type: "money_rest",
      value: 10000,
    },
    {
      num: "12",
      name: "クレジットカードを紛失．20000円払う．",
      type: "money",
      value: -20000,
    },
    {
      num: "13",
      name: "タンスの中からへそくりが見つかる．40000円もらう．",
      type: "money",
      value: 40000,
    },
    {
      num: "14",
      name: "お正月ハガキ宝くじで当選．20000円もらう．",
      type: "money",
      value: 20000,
    },
    {
      num: "15",
      name: "自然災害で自宅が被害にあう．100000円払う",
      type: "money",
      value: -100000
    }
  ]

let event50 =
  [
    {
      num: "0",
      type: null
    },
    {
      num: "1",
      name: "TikTokをやったら大ウケ．10000円もらう．",
      type: "money",
      value: 10000
    },
    {
      num: "2",
      name: "ぼーっと生きてんじゃねぇーよ！39000円払う．",
      type: "money",
      value: -39000
    },
    {
      num: "3",
      name: "お出かけ途中に悪質タックルにあう．1回休むが，30000円もらう．",
      type: "money_rest",
      value: 30000
    },
    {
      num: "4",
      name: "飲みの場でひょっこりしたらドン引きされる．20000円払う．",
      type: "money",
      value: -20000
    },
    {
      num: "5",
      name: "",
      type: "money",
      value: 40000
    },
    {
      num: "6",
      name: "旅行先でJCBギフト券を拾う．9000円もらう．",
      type: "money",
      value: 9000
    },
    {
      num: "7",
      name: "ポッキーの日にトッポを食べて有罪．20000円払う．",
      type: "money",
      value: -20000
    },
    {
      num: "8",
      name: "お隣さんと一緒に帰って噂される．20000円払う．",
      type: "money",
      value: -35000
    },
    {
      num: "9",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "10",
      name: "子供社会人イベント(結婚して子供がいる人のみ)",
      type: "adult"
    },
    {
      num: "11",
      name: "有給休暇を満喫．1ターン休むが10000円もらう．",
      type: "money_rest",
      value: 10000,
    },
    {
      num: "12",
      name: "クレジットカードを紛失．20000円払う．",
      type: "money",
      value: -20000,
    },
    {
      num: "13",
      name: "タンスの中からへそくりが見つかる．40000円もらう．",
      type: "money",
      value: 40000,
    },
    {
      num: "14",
      name: "お正月ハガキ宝くじで当選．20000円もらう．",
      type: "money",
      value: 20000,
    },
    {
      num: "15",
      name: "煽り運転容疑で逮捕，保釈金245000円払う．",
      type: "money",
      value: -245000
    }
  ]
```

## `/public/javascript/grid.js`

```js
let grid20 = [
  { x: 170, y: 90 },
  { x: 270, y: 100 },
  { x: 345, y: 100 },
  { x: 345, y: 175 },
  { x: 345, y: 250 },
  { x: 270, y: 250 },
  { x: 270, y: 325 },
  { x: 270, y: 400 },
  { x: 345, y: 400 },
  { x: 420, y: 400 },
  { x: 495, y: 400 },
  { x: 570, y: 400 },
  { x: 570, y: 325 },
  { x: 570, y: 250 },
  { x: 570, y: 175 },
  { x: 645, y: 175 },
  { x: 735, y: 175 },
];

let grid30 = [
  { x: 170, y: 100 },
  { x: 170, y: 205 },
  { x: 170, y: 280 },
  { x: 170, y: 355 },
  { x: 245, y: 355 },
  { x: 320, y: 355 },
  { x: 395, y: 355 },
  { x: 395, y: 280 },
  { x: 395, y: 205 },
  { x: 470, y: 205 },
  { x: 545, y: 205 },
  { x: 620, y: 205 },
  { x: 620, y: 280 },
  { x: 695, y: 280 },
  { x: 770, y: 280 },
  { x: 770, y: 205 },
  { x: 770, y: 90 },
];

let grid40 = [
  { x: 110, y: 105 },
  { x: 205, y: 105 },
  { x: 280, y: 105 },
  { x: 355, y: 105 },
  { x: 430, y: 105 },
  { x: 430, y: 180 },
  { x: 505, y: 180 },
  { x: 505, y: 255 },
  { x: 505, y: 330 },
  { x: 505, y: 405 },
  { x: 580, y: 405 },
  { x: 655, y: 405 },
  { x: 730, y: 405 },
  { x: 730, y: 330 },
  { x: 730, y: 255 },
  { x: 730, y: 180 },
  { x: 730, y: 80 },
];

let grid50 = [
  { x: 440, y: 100 },
  { x: 345, y: 100 },
  { x: 270, y: 100 },
  { x: 195, y: 100 },
  { x: 195, y: 175 },
  { x: 195, y: 250 },
  { x: 195, y: 325 },
  { x: 270, y: 325 },
  { x: 345, y: 325 },
  { x: 420, y: 325 },
  { x: 420, y: 400 },
  { x: 495, y: 400 },
  { x: 570, y: 400 },
  { x: 645, y: 400 },
  { x: 645, y: 325 },
  { x: 645, y: 250 },
  { x: 645, y: 150 },
];
```

## `/public/javascript/jobs.js`

```js
let student = [
  {
    name: "卓越した学生"
  }
]

let baseball = [
  {
    name: "新人",
    salary: 15000
  },

  {
    name: "2軍選手",
    salary: 30000
  },

  {
    name: "2軍首位打者",
    salary: 45000
  },

  {
    name: "1軍レギュラー",
    salary: 95000
  },

  {
    name: "トリプルスリー",
    salary: 155000
  }
];

let chef = [
  {
    name: "見習い",
    salary: 10000
  },

  {
    name: "二流シェフ",
    salary: 15000
  },

  {
    name: "ホテルシェフ",
    salary: 30000
  },

  {
    name: "三つ星ホテルシェフ",
    salary: 75000
  },

  {
    name: "総料理長",
    salary: 120000
  }
]


let doctor = [
  {
    name: "研修医",
    salary: 5000
  },

  {
    name: "若手医師",
    salary: 10000
  },

  {
    name: "中堅医師",
    salary: 15000
  },

  {
    name: "lv(ベテラン医師",
    salary: 50000
  },

  {
    name: "院長",
    salary: 200000
  }
]

let engineer = [
  {
    name: "平社員",
    salary: 5000
  },

  {
    name: "主任",
    salary: 7500
  },

  {
    name: "課長",
    salary: 10000
  },

  {
    name: "部長",
    salary: 15000
  },

  {
    name: "執行役員",
    salary: 20000
  }
];

let musician = [
  {
    name: "路上歌手",
    salary: 2000
  },

  {
    name: "ミニヒット歌手",
    salary: 5000
  },

  {
    name: "CDデビュー歌手",
    salary: 7500
  },

  {
    name: "ライブ歌手",
    salary: 15000
  },

  {
    name: "Mステ出場歌手",
    salary: 30000
  }
];

let teacher = [
  {
    name: "講師",
      salary: 45000
  },

  {
    name: "教諭",
      salary: 46000
  },

  {
    name: "主任教諭",
    salary: 47000
  },

  {
    name: "教頭",
    salary: 48000
  },

  {
    name: "校長",
    salary: 50000
  }
];
```

## `/stylesheets/animation.css`

```css
#animation {
  display: none;
}
#animation:after{  
  content: "";
  position: absolute;
  left: 0;
  top: 150px;
  height: 300px;
  background-color:#F9CE00;
  animation: secondaryImageOverlayIn 0.6s 0s cubic-bezier(.77,0,.175,1),        
             secondaryImageOverlayOut 0.6s 0.6s cubic-bezier(.77,0,.175,1);
  animation-fill-mode: both;
}
#animeText {
  position: absolute;
  width: 100%;
  text-align: center;
  font-weight: 600;
  font-size: 170px;
  color: white;
  -webkit-text-stroke: 4px rgb(34, 5, 5);
  animation:fadeIn 0.1s 0.5s;
  animation-fill-mode: both;
}

.fadeout {
  animation : fadeOut 1s;
  animation-fill-mode: both;
}
@keyframes fadeOut {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}

@keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
@keyframes secondaryImageOverlayIn {
    0% {
      width: 0;
    }
    100% {
      width:  100%;
    }
  }
  @keyframes secondaryImageOverlayOut {
    0% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(130%);
    }
  }

```

## `/stylesheets/game.css`

```css
textarea{
  float: left;
  width: 540px;
  height: 95%;
  background-color: #2E4025;
  color: #E8F7DD;
  border-left: 1px solid white;
  resize: none;
}

img{
  position: absolute;
}

.boxes{
  width: 1340px;
  display: table;
  position: absolute;
}

.statusBox{
  display: table-cell;
  vertical-align:middle;
}

.box{
  width: 200px;
  height: 270px;
  margin: 10px;
}

#box1{
  background-color: rgba(0, 38, 255 ,0.3);
}

#box1:hover{
  background-image: url("../image/playerBackground/bigDenden.png");
}

#box2{
  background: rgba(252, 27, 222, 0.3);
}
#box2:hover{
  background-image: url("../image/playerBackground/bigUsagi.png");
}

#box3{
  background: rgba(255, 36, 65, 0.3);
}

#box3:hover{
  background-image: url("../image/playerBackground/bigKani.png");
}

#box4{
  background: rgba(245, 221, 2, 0.3);
}

#box4:hover{
  background-image: url("../image/playerBackground/bigHiyoko.png");
}

.game_field{
  position: relative;
  background-image: url("../image/background/background20.png");
  display: block;
  overflow: hidden;
  margin-top: 0px;

  width: 900px;
  height: 500px;
  z-index: 0;
}

.bottom-menu{
  position: relative;
  float: left;
  font-family: sans-serif;
  background-color: #2E4025;
  color: #E8F7DD;
  border: 1px solid #D1F5BD;
  left: 50%;
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
  width: 900px;
  height: 100px;
  z-index: 0;
}
button{
  display: inline;
  margin: 0%;
  background: transparent;
  cursor: pointer;
  border: none;
  color: #E8F7DD;
  outline: none;

  font-weight: 700;
}

button:hover{
  color: rgb(151, 206, 120);
}

button:active{
  -ms-transform: translateY(4px);
  -webkit-transform: translateY(4px);
  transform: translateY(4px);/*下に動く*/
}

table{
  border: none;
  width: 100%;
  height: 100%;
  text-align: center;
  padding: 0px;
}
tr td{
  padding: 0%;
  height: 40px;
}

.controlPanel{
  float: left;
  width: 354px;
  height: 100px;
}

#dice{
  position: absolute;
  display: none;
  width: 100px;
  top: 150px;
  left: 50px;
}
/* 画面サイズが小さくなっても正しく表示出来る用 */
@media screen and (min-width: 1340px){
  .boxes{
    margin: 0 0 0 -670px;  /*縦横半分をネガティブマージンでずらす*/
    position: absolute;     /*body要素に対して絶対配置*/
    left: 50%;      /*左端を中央に*/
  }
}

/* 画面サイズが小さくなっても正しく表示出来る用 */
@media screen and (max-width: 1340px){
  iframe{
    width: 1341px;
  }
}

```

## `/stylesheets/header.css`

```css
:root {
  --button-color: #64abe8;
  --button-dark: #3794E1;
  --button-light: #BCDBF5;
  --lightlight: #E8F3FB;
  --background-header: rgba(250, 250, 250, 0.7);
  --shadow-color: rgba(62, 62, 62, 0.13);
}

.inner {
  background-color: #f8f8f8;/*#E3EEFF;*/
  text-align: center;
  position: relative;
}

/* -----------------------------
    header
------------------------------*/
#header {
  position: relative;
  left: 0px;
  padding-top: 25px;
  padding-bottom: 25px;
  background: var(--background-header);/*#7087bb;*/
  box-shadow: 0px 3px 2.7px 0.3px var(--shadow-color);
}

#header .title {
  height: 40px;
}

#header .logo {
  width: 50px;
  height: 50px;
}
```

## `/stylesheets/register.css`

```css
input[type="text"]:focus,
textarea:focus {
  box-shadow: 0 0 7px #E8FBE5;
  border: 1px solid #E8FBE5;
}
h3{
  text-align: center;
  position: relative;
  color: green;
}
.register_form{
  text-align: center;
  position: relative;
  color: #335E1D;
  border: none;
  border-radius: 0;
  outline: none;
}
```

## `/stylesheets/style.css`

```css
iframe{
  margin: 0px;
  width: 100%;
  border: 0px;
}

body {
  margin: 0px;
  font: 14px;
  font-family: serif;
  background-color: #E8FBE5;
}


h1{
  position: relative;
  top: 0%;
  margin-left: 50%;
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
  /*padding: 10px;*/
  color: #335E1D;
}


.square_btn {
    position: relative;
    left: 50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
    display: inline-block;
    font-weight: bold;
    padding: 7px 10px 10px 10px;
    text-decoration: none;
    color: #FFF;
    background: #76BB3D;
    transition: .4s;
}

.square_btn>span {
    border-bottom: solid 2px #FFF;
}

.square_btn:hover {
    background: #91b5fb;
}

```
