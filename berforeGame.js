enchant(); // enchant.js を使うという宣言みたいなもの
let times = 0; // ボタンを押した回数

let dice_flag = 0; //0:さいころ停止 1:さいころ回転中 2:さいころ結果
const PLAYER_NUMBER = 4; //プレイヤー数

const DECIDEJOB = 0; //就職
const PLAYDICE = 1; //すごろく
const CARRIERUP = 2; //昇給
const ENDING = 3;
const SLEEPTIME = 500;
let gamestate = DECIDEJOB; //gamestateによってサイコロを振った後の処理が変わる

let stage=0; //ステージレベル数
let turn=0; //ターン数

let count = 0;

/* ボタンが押されたときの処理 */
const click_btn = () => {
  times += 1;
  document.getElementById("area").value += "\n" + times + " times click !";
  goBottom("area");
}

/* テキストエリアの最下へ移動するための関数 */
const goBottom = (targetId) => {
  let obj = document.getElementById(targetId);
  if(!obj) return;
  obj.scrollTop = obj.scrollHeight;
}

//ループ内でのスリープ
const loopSleep = (_loopLimit,_interval, _mainFunc) => {
  let loopLimit = _loopLimit;
  let interval = _interval;
  let mainFunc = _mainFunc;
  let i = 0;
  let loopFunc = () => {
    let result = mainFunc(i);
    if (result === false) {
      // break機能
      return;
    }
    i = i + 1;
    if (i < loopLimit) {
      setTimeout(loopFunc, interval);
    }
  }
  loopFunc();
}

//ます
const GRID_MAX = 30; //マスの数
//手動調節（要調整）
let grid = [{x: 150, y: 65},
            {x: 230, y: 65},
            {x: 290, y: 65},
            {x: 350, y: 65},
            {x: 350, y: 125},
            {x: 350, y: 185},
            {x: 350, y: 245},
            {x: 350, y: 305},
            {x: 290, y: 305},
            {x: 230, y: 305},
            {x: 170, y: 305},
            {x: 170, y: 365},
            {x: 170, y: 420},
            {x: 230, y: 420},
            {x: 290, y: 420},
            {x: 350, y: 420},
            {x: 410, y: 420},
            {x: 470, y: 420},
            {x: 530, y: 420},
            {x: 530, y: 360},
            {x: 530, y: 300},
            {x: 530, y: 240},
            {x: 590, y: 240},
            {x: 650, y: 240},
            {x: 650, y: 300},
            {x: 710, y: 300},
            {x: 770, y: 300},
            {x: 770, y: 240},
            {x: 770, y: 180},
            {x: 770, y: 120},
            {x: 770, y: 60},
          ];

//ステータスの表示（デバッグ用）
const dispState = () => {
  document.getElementById("stage").innerHTML = "20代";
  document.getElementById("turn").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".name;");
  document.getElementById("gold").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".gold;");
  document.getElementById("grid").innerHTML = GRID_MAX-eval("piece"+(turn%PLAYER_NUMBER)+".grid;");
  document.getElementById("job").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".job;");
  document.getElementById("joblevel").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".joblevel;");
  document.getElementById("rank").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".rank;");
}

//ステージの更新
const updateStage = () => {
  stage++;

  if(stage>3){
    gamestate = ENDING;
  }else{
    Jsongene();
    document.getElementById("stage").innerHTML = stage*10+20 + "代";
    document.getElementById("area").value += "\n" + (stage*10+20) + "代";
    goBottom("area");
  }
}

const payday = () => {
  animation();
  for (let i = 0; i < PLAYER_NUMBER; i++) {
    eval("piece" + i + ".addSalary()");
  }
}
//ターンの更新
const updateTurn = () => {
  turn++;
  console.log("turn = " + turn);

  if(turn%8 == 0 && turn > 1){
    payday();
  }
  document.getElementById("turn").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".name;");
  document.getElementById("gold").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".gold;");
  document.getElementById("grid").innerHTML = GRID_MAX-eval("piece"+(turn%PLAYER_NUMBER)+".grid;");
  document.getElementById("job").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".job;");
  document.getElementById("joblevel").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".joblevel;");
  document.getElementById("rank").innerHTML = eval("piece"+(turn%PLAYER_NUMBER)+".rank;");

  document.getElementById("area").value += "\n" + eval("piece"+(turn%PLAYER_NUMBER)+".name;") + "のターン";
  goBottom("area");
}

//jsonの処理
let baseball = [];
let chef = [];
let doctor = [];
let engineer = [];
let musician = [];
let teacher = [];

let events = [];

const initJson = ()  => {
  $.getJSON(
    "../public/json/job/baseball.json",
    null,
    (data, status) => {
      baseball = data.baseball;
    });
  $.getJSON(
    "../public/json/job/chef.json",
    null,
    (data, status) => {
      chef = data.chef;
    });
  $.getJSON(
    "../public/json/job/doctor.json",
    null,
    (data, status) => {
      doctor = data.doctor;
    });
  $.getJSON(
    "../public/json/job/engineer.json",
    null,
    (data, status) => {
      engineer = data.engineer;
    });
  $.getJSON(
    "../public/json/job/musician.json",
    null,
    (data, status) => {
      musician = data.musician;
    });
  $.getJSON(
    "../public/json/job/teacher.json",
    null,
    (data, status) => {
      teacher = data.teacher;
    });
}

const Jsongene = () => {
  eval("var text = '../public/json/event/event'+ (stage*10+20) +'.json';");

  $.getJSON(
    text,
    null,
    (data, status) => {
      events = data.event;
    });
}

//こま
let Piece = function(name, image, num){
  this.name = name; //コマの名前
  this.image = image; //コマの画像のid
  this.size = 25; //コマの画像のサイズ
  this.num = num; //コマの番号
  this.rank = 1; //コマの順位
  this.grid = 0; //コマのマス位置
  this.x = grid[this.grid].x; //コマの座標x
  this.y = grid[this.grid].y; //コマの座標y
  this.gold = 1000; //コマの所持金
  this.job = "無職"; //コマの職
  this.jobnum = 0;   //職業番号
  this.jobName = ""; //職業の名前
  this.joblevel = 0; //コマのジョブレベル

  //コマの位置の初期化
  this.initPiece = () => {
    document.getElementById(this.image).style.display="block";
    this.grid = 0;
    const target = document.getElementById(this.image);
    target.style.transform = `translate( ${this.x}px, ${this.y}px )`;
  }

  //コマの名前の取得
  this.getName = () => {
    return this.name;
  }

  //コマのマス位置の取得
  this.getGrid = () => {
    return this.grid;
  }

  //コマの所持金の取得
  this.getGold = () => {
    return this.gold;
  }

  // 給料
  this.addSalary = () => {
    let salary = eval(this.jobName+"[this.joblevel].salary");
    console.log(salary);
    this.addGold(salary)
  }

  //コマの仕事の設定
  this.setJob = () => {
    switch(this.jobnum){
      case 1:
        this.job = baseball[this.joblevel].name;
        this.jobName = "baseball";
      break;
      case 2:
        this.job = chef[this.joblevel].name;
        this.jobName = "chef";
        break;
      case 3:
        this.job = doctor[this.joblevel].name;
        this.jobName = "doctor";
        break;
      case 4:
        this.job = engineer[this.joblevel].name;
        this.jobName = "engineer";
        break;
      case 5:
        this.job = musician[this.joblevel].name;
        this.jobName = "musician";
        break;
      case 6:
        this.job = teacher[this.joblevel].name;
        this.jobName = "teacher";
        break;
    }
    document.getElementById("area").value += "\n" + eval("piece"+(turn%PLAYER_NUMBER)+".name;")+"は"+this.job+ "になりました";
    goBottom("area");

  }

  this.setJobnum = (num) => {
    this.jobnum = num;
  }

  //駒の表示・非表示（0:非表示 1:表示）
  this.show = (num) => {
    if (num)
      document.getElementById(this.image).style.display="block";
    else
      document.getElementById(this.image).style.display="none";
  }

  //駒の移動（引数にますの移動量-1~1）
  this.moveGrid = (movegridnum) => {
    this.grid+=movegridnum;
    if(this.grid<0)
      this.grid = 0;
    else if(this.grid<GRID_MAX+1){
      this.x = grid[this.grid].x;
      this.y = grid[this.grid].y;
      switch(this.num){
        case 1 :
          this.x+=this.size;
          this.y-=this.size;
        break;
        case 2 :
          this.y-=this.size;
        break;
        case 3 :
          this.x+=this.size;
          this.y-=50;
        break;
        default :
        break;
      }
      const target = document.getElementById(this.image);
      target.style.transform = `translate( ${this.x}px, ${this.y}px )`;
    }
  }

  //金の変動（引数に変動量）
  this.addGold = (gold) => {
    this.gold+=gold;
  }
}

//全コマの位置の初期化
const initAllPiece = () => {
  for(let i=0; i<PLAYER_NUMBER; i++){
    eval("piece"+i+".initPiece();");
    eval("piece"+i+".moveGrid(0);");
  }
}

//マスのイベント処理
const grid_event = (event) => {
  document.getElementById("area").value += "\n" + event.text;
  goBottom("area");
  switch(event.type){
    case "gold" :
      eval("piece"+(turn%PLAYER_NUMBER)+".addGold(event.value);");
    break;
    case "jump" :
    break;
    case "love" :
    break;
    case "revenge" :
    break;
    case "gold_rest" :
    break;
    default :
    break;
  }
}

//コマの順位決定
const checkRank = () => {
  let check = [{player:0,grid:GRID_MAX-piece0.grid,rank:0},
               {player:1,grid:GRID_MAX-piece1.grid,rank:0},
               {player:2,grid:GRID_MAX-piece2.grid,rank:0},
               {player:3,grid:GRID_MAX-piece3.grid,rank:0}];

  let min;
  let temp;

  for(let i=0; i<PLAYER_NUMBER-1; i++){
    min = i;
    for(let j=i+1; j<PLAYER_NUMBER; j++){
      if (check[j].grid < check[min].grid)
        min = j; // 最小値を持つ要素を更新
    }
    temp = check[i];
    check[i] = check[min];
    check[min] = temp;
  }

  for(let i=0; i<PLAYER_NUMBER; i++){
    switch(i){
      case 0:
        check[i].rank = 1;
      break;
      case 1:
      case 2:
      case 3:
        if(check[i].grid==check[i-1].grid){
          check[i].rank = check[i-1].rank;
        }
        else
          check[i].rank = check[i-1].rank+1;
      break;
      default:
      break;
    }
  }
  for(let i=0; i<PLAYER_NUMBER; i++){
    eval("piece"+check[i].player+".rank=check[i].rank");
  }
}

const levelUptext = (rank) => {
  switch(rank){
    case 1:
      document.getElementById("area").value += "\n" + "1,2,3,4 : 2up";
      document.getElementById("area").value += "\n" + "5,6 : 1up";
      goBottom("area");
    break;
    case 2:
      document.getElementById("area").value += "\n" + "1,2,3 : 2up";
      document.getElementById("area").value += "\n" + "4,5,6 : 1up";
      goBottom("area");
    break;
    case 3:
      document.getElementById("area").value += "\n" + "1,2 : 2up";
      document.getElementById("area").value += "\n" + "3,4,5,6 : 1up";
      goBottom("area");
    break;
    case 4:
      document.getElementById("area").value += "\n" + "1 : 2up";
      document.getElementById("area").value += "\n" + "2,3,4,5,6 : 1up";
      goBottom("area");
    break;
  }
}

//コマのレベルを調べて昇級
const upCarrier = (dice_num) => {
  switch(eval("piece"+(turn%PLAYER_NUMBER)+".rank;")){
    case 1:
      switch(dice_num){
        case 1:
          return 2;
          break;
        case 2:
          return 2;
        break;
        case 3:
          return 2;
        break;
        case 4:
          return 2;
        break;
        case 5:
          return 1;
        break;
        case 6:
          return 1;
        break;
        default:
        break;
      }
    break;
    case 2:
      switch(dice_num){
        case 1:
          return 2;
        break;
        case 2:
          return 2;
        break;
        case 3:
          return 2;
        break;
        case 4:
          return 1;
        break;
        case 5:
          return 1;
        break;
        case 6:
          return 1;
        break;
        default:
        break;
      }
    break;
    case 3:
      switch(dice_num){
        case 1:
          return 2;
        break;
        case 2:
          return 2;
        break;
        case 3:
          return 1;
        break;
        case 4:
          return 1;
        break;
        case 5:
          return 1;
        break;
        case 6:
          return 1;
        break;
        default:
        break;
      }
    break;
    case 4:
      switch(dice_num){
        case 1:
          return 2;
        break;
        case 2:
          return 1;
        break;
        case 3:
          return 1;
        break;
        case 4:
          return 1;
        break;
        case 5:
          return 1;
        break;
        case 6:
          return 1;
        break;
        default:
        break;
      }
    break;
    default :
      return 0;
    break;
  }
}

const jobleveltext = () => {
  document.getElementById("area").value += "\n" + (stage*10+20) + "代";
  document.getElementById("area").value += "\n" + "昇給イベント";
  document.getElementById("area").value += "\n" + "サイコロの目で職業レベルがアップ";
  levelUptext(eval("piece"+(turn%PLAYER_NUMBER)+".rank"));
  document.getElementById("area").value += "\n" + eval("piece"+(turn%PLAYER_NUMBER)+".name;")+"のターン";
  goBottom("area");

}

//コマの生成
let piece0 = new Piece(window.sessionStorage.getItem("name1"), "face0", 0);
let piece1 = new Piece(window.sessionStorage.getItem("name2"), "face1", 1);
let piece2 = new Piece(window.sessionStorage.getItem("name3"), "face2", 2);
let piece3 = new Piece(window.sessionStorage.getItem("name4"), "face3", 3);

/* 画面がロードされてからの処理 */
window.onload = () => {

  initAllPiece();
  initJson();
  Jsongene();
  dispState();

  let move_num;

  // appという変数の中にVueのインスタンスを作成
  let app = new Vue({
    el: '#app',
    data: {　//Vue.jsの変数はdata内に記述する
      menuOptions: ["サイコロ","ステータス","現在の順位","設定"],
      title: "すごろくは20歳になってから",
      // 前のページからの値の受取はsessionSttageを使用する
      name1: window.sessionStorage.getItem("name1"),
      name2: window.sessionStorage.getItem("name2"),
      name3: window.sessionStorage.getItem("name3"),
      name4: window.sessionStorage.getItem("name4"),
    },
    methods: { // Vue.jsの関数はmethod内に記述する
      /* メニューを押したときの処理 */
      click_menu:(option) => {
        switch (option) {
          case 1:
            if(!dice_flag)
              dice_flag=1;
            else if(dice_flag==1)
              dice_flag=2;
          break;
          case 2:
            document.getElementById("area").value += "\n" + "プレイヤー: " + eval("piece"+(turn%PLAYER_NUMBER)+".name;");
            document.getElementById("area").value += "\n" + "所持金: " + eval("piece"+(turn%PLAYER_NUMBER)+".gold;");
            document.getElementById("area").value += "\n" + "残りマス: " + (GRID_MAX-eval("piece"+(turn%PLAYER_NUMBER)+".grid;")-1);
            document.getElementById("area").value += "\n" + "職業: " + eval("piece"+(turn%PLAYER_NUMBER)+".job;");
            document.getElementById("area").value += "\n" + "職業レベル " + eval("piece"+(turn%PLAYER_NUMBER)+".joblevel;");
            goBottom("area");
          break;
          case 3:
            document.getElementById("area").value += "\n" + "clicked " + this.menuOptions[2];
            document.getElementById("area").value += "\n" + piece0.name + ":" + piece0.rank+"位";
            document.getElementById("area").value += "\n" + piece1.name + ":" + piece1.rank+"位";
            document.getElementById("area").value += "\n" + piece2.name + ":" + piece2.rank+"位";
            document.getElementById("area").value += "\n" + piece3.name + ":" + piece3.rank+"位";
            goBottom("area");
          break;
          case 4:
            document.getElementById("area").value += "\n" + "clicked " + this.menuOptions[3];
            goBottom("area");
          break;
          default:
            console.log("error");
          break;
        }
      },
      //オープニング処理
      opening: () => {
        document.getElementById("area").value += "\n" + "すごろくは20歳になってから";
        document.getElementById("area").value += "\n" + "職業決め";
        document.getElementById("area").value += "\n" + "1:野球選手";
        document.getElementById("area").value += "\n" + "2:料理人";
        document.getElementById("area").value += "\n" + "3:医者";
        document.getElementById("area").value += "\n" + "4:技術者";
        document.getElementById("area").value += "\n" + "5:音楽家";
        document.getElementById("area").value += "\n" + "6:教師";

        document.getElementById("area").value += "\n" +"サイコロを振って職業を決める";

        document.getElementById("area").value += "\n" + app.name1 + "のターン";
        goBottom("area");
      },
      //エンディング処理
      ending: () => {
          gamestate = ENDING;
      },
      /* プレイヤー名をすべてテキストエリアに表示(デバック用) */
      print_name: () => {
        document.getElementById("area").value += "\n" + "Player1 : " + app.name1;
        goBottom("area");
        document.getElementById("area").value += "\n" + "Player2 : " + app.name2;
        goBottom("area");
        document.getElementById("area").value += "\n" + "Player3 : " + app.name3;
        goBottom("area");
        document.getElementById("area").value += "\n" + "Player4 : " + app.name4;
        goBottom("area");
      },
      /* さいころが振り終わったときの処理 */
      dice_end: (dice_num)  => {
        document.getElementById("area").value += "\n" + "さいころの数字は " + dice_num + " です！";
        goBottom("area");
        // setTimeout内の第2引数[ms]経つと第1引数が実行
        setTimeout(() => {
          // さいころを非表示設定
          document.getElementById("enchant-stage").style.display = "none";
        }, 1000);

        switch(gamestate){
          case DECIDEJOB :
            setTimeout(() => {
              updateTurn()
            }, 1000);
            setTimeout(() => {
              dice_flag=0;
            }, 1000);

            eval("piece"+(turn%PLAYER_NUMBER)+".setJobnum(dice_num);");
            eval("piece"+(turn%PLAYER_NUMBER)+".setJob();");

            if(turn==PLAYER_NUMBER-1){
              gamestate = PLAYDICE;
              document.getElementById("area").value += "\n" + "すごろく開始";
              goBottom("area");
            }
          break;
          case PLAYDICE :
            //ゴール判定
            if(dice_num<GRID_MAX-eval("piece"+(turn%PLAYER_NUMBER)+".getGrid();")){
              move_num = dice_num;
              setTimeout(() => {
                updateTurn()
              }, (move_num+1)*SLEEPTIME);
            }else{
              move_num = GRID_MAX-eval("piece"+(turn%PLAYER_NUMBER)+".getGrid();");
              setTimeout(() => {
                initAllPiece();
              }, (move_num+1)*SLEEPTIME);
              setTimeout(() => {
                jobleveltext();
              }, (move_num+1)*SLEEPTIME);
              goBottom("area");
              gamestate = CARRIERUP;
            }

            //コマ移動
            setTimeout(() => {
              loopSleep(move_num, SLEEPTIME, (i) => {
                eval("piece"+(turn%PLAYER_NUMBER)+".moveGrid(1);");
              });
            }, SLEEPTIME);

            let grid = eval("piece"+(turn%PLAYER_NUMBER)+".grid;")+move_num;
            setTimeout(() => {
              grid_event(events[grid]);
            }, move_num*SLEEPTIME);
            setTimeout(() => {
              checkRank();
            }, (move_num)*SLEEPTIME);
            //さいころの振り判定
            setTimeout(() => {
              dice_flag=0;
            }, (move_num+1)*SLEEPTIME);
          break;
          case CARRIERUP :
            setTimeout(() => {
              dice_flag=0;
            }, SLEEPTIME);
            setTimeout(() => {
              updateTurn()
            }, SLEEPTIME);

            let levelnum;
            levelnum = upCarrier(dice_num);

            if(eval("piece"+(turn%PLAYER_NUMBER)+".joblevel;")==4){
              document.getElementById("area").value += "\n" + "職業レベルマックス";
              document.getElementById("area").value += "\n" + (10000*levelnum)+"gold獲得";
              eval("piece"+(turn%PLAYER_NUMBER)+".addGold(10000*levelnum);");
              goBottom("area");
            }else if(eval("piece"+(turn%PLAYER_NUMBER)+".joblevel+levelnum;")>4){
              eval("piece"+(turn%PLAYER_NUMBER)+".joblevel=4;");
              eval("piece"+(turn%PLAYER_NUMBER)+".setJob();");
            }else{
              eval("piece"+(turn%PLAYER_NUMBER)+".joblevel+=levelnum;");
              eval("piece"+(turn%PLAYER_NUMBER)+".setJob();");
            }
            if(count==PLAYER_NUMBER-1){
              count = 0;
              updateStage();
              gamestate = PLAYDICE;
            }else{
              setTimeout(() => {
                levelUptext(eval("piece"+(turn%PLAYER_NUMBER)+".rank"));
              }, 500);
              count++;
            }
          break;
        }
    }
    },
  });　//ここまでVue

  // Vue内の関数を外から呼び出すときはapp.関数名()で呼び出す
  // app.print_name();
  app.opening();

  /* ここから enchant.js */
  let game = new Game(320,220); //画面サイズ指定
  game.fps = 30;
  let dice = new Sprite(200,200);// diceのサイズ
  dice.scale(0.3,0.3); //大きさを0.3倍
  let frame = 0;

  // 画像読み込み
  game.preload('../public/image/dice.png');
  game.preload('../public/image/1.png');
  game.preload('../public/image/2.png');
  game.preload('../public/image/3.png');
  game.preload('../public/image/4.png');
  game.preload('../public/image/5.png');
  game.preload('../public/image/0.png');
  /* 準備が終わったら読み込まれる関数 */
  game.onload = () => {
    /* 振る前の初期化関数 */
    const dice_init = () => {
      game.fps = 30;
      dice.image = game.assets['../public/image/dice.png'];
      dice.x = 0;
      dice.y = 0;
      dice.initflag = true;
      dice.frame = 7;
      frame = 0;
    }

    dice_init();
    game.rootScene.addChild(dice); //画像表示

    /* 新しいフレームになったら反応する */
    game.addEventListener('enterframe' , ()  => { // 荒れてる

      if(!dice_flag){
        if(!dice.initflag)
          dice_init();
      }else if(dice_flag==1){
        dice.initflag = false;
        document.getElementById("enchant-stage").style.display = "block";
        frame += 1;
        frame %= 5;
        dice.frame = frame + 6;
        dice.rotate(Math.floor( Math.random() * 360 ));
      }else if(game.fps>=0){
        dice.rotation = 0;
        game.fps -= 2;
        frame += 1;
        frame %= 5;
        dice.x += 8;
        dice.y += 4;
        dice.frame = frame + 6;
        if(game.fps == 0){
          let dice_num = Math.floor( Math.random() * 6 ); // 0 ~ 5
          dice.image = game.assets['../public/image/' + dice_num + '.png'];
          dice_num += 1; //1 ~ 6
          app.dice_end(dice_num);
        }
      }
    });
  }
  // enchantの実行
  if(!(gamestate==ENDING)){
    game.start();
  }
};
